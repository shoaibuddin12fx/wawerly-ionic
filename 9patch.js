#!/usr/bin/node

const fs  = require('fs');
const PNG = require('pngjs').PNG;

const sizes = [
  'mdpi',//     320 x  480
  'hdpi',//     480 x  800
  'xhdpi',//    720 x 1280
  'xxhdpi',//   960 x 1600
  'xxxhdpi',// 1280 x 1920
];

sizes.forEach(async size => {
  fs.createReadStream(`android/app/src/main/res/drawable-${size}/.splash.png`)
    .pipe(new PNG())
    .on('parsed', function () {

      const dst = new PNG({width: this.width + 2, height: this.height + 2, inputHasAlpha: true});

      for (let y = 0; y < this.height; y++) {
        for (let x = 0; x < this.width; x++) {
          const srcidx = (this.width * y + x) << 2;
          const dstidx = (dst.width * (y + 1) + (x + 1)) << 2;

          // set color
          dst.data[dstidx]     = this.data[srcidx];
          dst.data[dstidx + 1] = this.data[srcidx + 1];
          dst.data[dstidx + 2] = this.data[srcidx + 2];

          // set opacity
          dst.data[dstidx + 3] = 255;
        }
      }

      for (let y = 0; y < this.height; y++) {
        let rowIsWhite = true;
        for (let x = 0; x < this.width; x++) {
          const srcidx = (this.width * y + x) << 2;
          if (this.data[srcidx] < 255 || this.data[srcidx + 1] < 255 || this.data[srcidx + 2] < 255) {
            rowIsWhite = false;
            break;
          }
        }
        if (rowIsWhite) {
          const dstidxstart         = (dst.width * (y + 1)) << 2;
          const dstidxend           = (dst.width * (y + 1) + (dst.width - 1)) << 2;
          dst.data[dstidxstart]     = dst.data[dstidxstart + 1] = dst.data[dstidxstart + 2] = 0;
          dst.data[dstidxstart + 3] = 255;
          dst.data[dstidxend]       = dst.data[dstidxend + 1] = dst.data[dstidxend + 2] = 0;
//          dst.data[dstidxend + 3]   = 255;
        }
      }

      let xstart = -1, xend = this.width;
      for (let x = 0; x < this.width; x++) {
        let colIsWhite = true;
        for (let y = 0; y < this.height; y++) {
          const srcidx = (this.width * y + x) << 2;
          if (this.data[srcidx] < 255 || this.data[srcidx + 1] < 255 || this.data[srcidx + 2] < 255) {
            colIsWhite = false;
            break;
          }
        }
        if (!colIsWhite) {
          if (xstart < 0) {
            xstart = x;
          } else {
            xend = x;
          }
        }
      }
      for (let x = 0; x < xstart; x++) {
        const dstidxstart         = ((x + 1)) << 2;
        const dstidxend           = (dst.width * (dst.height - 1) + (x + 1)) << 2;
        dst.data[dstidxstart]     = dst.data[dstidxstart + 1] = dst.data[dstidxstart + 2] = 0;
        dst.data[dstidxstart + 3] = 255;
        dst.data[dstidxend]       = dst.data[dstidxend + 1] = dst.data[dstidxend + 2] = 0;
//          dst.data[dstidxend + 3]   = 255;
      }
      for (let x = xend + 1; x < this.width; x++) {
        const dstidxstart         = ((x + 1)) << 2;
        const dstidxend           = (dst.width * (dst.height - 1) + (x + 1)) << 2;
        dst.data[dstidxstart]     = dst.data[dstidxstart + 1] = dst.data[dstidxstart + 2] = 0;
        dst.data[dstidxstart + 3] = 255;
        dst.data[dstidxend]       = dst.data[dstidxend + 1] = dst.data[dstidxend + 2] = 0;
//          dst.data[dstidxend + 3]   = 255;
      }

      dst.pack()
         .pipe(fs.createWriteStream(`android/app/src/main/res/drawable-${size}/splash.9.png`));
    });
});
