#!/bin/bash

PHONE_IP=10.27.164.118

set -e
set -u

export GENERATE_SOURCEMAP=false
npx ionic capacitor sync --prod --platform=android
#https://forum.ionicframework.com/t/how-to-build-apk-from-capacitor-ionic-poject-without-using-android-studio/177814/12
cd android
./gradlew --no-daemon assembleRelease
cd app/build/outputs/apk/release
jarsigner -keystore ../../../../../../keystore.keystore -storepass 352DeJsgtxG.6 app-release-unsigned.apk waverley.transport.maps.transit.timetable -keypass AmJyr.sjVp4IM
zipalign -f 4 app-release-unsigned.apk app-release.apk
[[ $(adb devices | wc -l) -gt 3 ]] && adb disconnect $PHONE_IP:5555
# in case USB is flaky, start a network server with `adb tcpip 5555`
adb devices | grep device$ || adb connect $PHONE_IP:5555
#adb uninstall waverley.transport.maps.transit.timetable
adb install app-release.apk
adb shell am start -n waverley.transport.maps.transit.timetable/waverley.transport.maps.transit.timetable.MainActivity -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
git tag "a$(date +%Y%m%d%H)"
