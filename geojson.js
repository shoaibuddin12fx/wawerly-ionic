#!/usr/bin/env nodejs --harmony-optional-chaining

const fs = require('fs');

(async () => {
  const pkg       = require('./public/assets/data/package.json');
  const zip2shp   = require('./public/assets/data/zip2shp.json');
  const resources = pkg.result[0].resources.map((resource) => {
    const name    = resource.name;
    const zip     = resource.url.replace(/.*\//, '');
    const geojson = zip2shp[zip]?.replace(/.shp/, '.json');
    return {name, geojson};
  }).filter((resource) => {
    return resource.geojson;
  });

  // convert osm to geojson
  const overpass         = require('./public/assets/data/osm.json');
  const overpassFeatures = overpass.elements.map(({lat, lon, ...properties}) => ({
    type:       'Feature',
    'geometry': {
      type:        'Point',
      coordinates: [lon, lat],
      properties:  properties,
    },
  }));
  const overpassData     = {type: 'FeatureCollection', features: overpassFeatures};
  fs.writeFileSync('./public/assets/data/geojson/osm.json', JSON.stringify(overpassData, undefined, 1));
  resources.push({name: 'Charging stations (OSM)', geojson: 'osm.json'});

  // convert ocm to geojson
  const ocm         = require('./public/assets/data/ocm.json');
  const ocmFeatures = ocm.map((element) => ({
    type:       'Feature',
    'geometry': {
      type:        'Point',
      coordinates: [element.AddressInfo.Longitude, element.AddressInfo.Latitude],
      properties:  element,
    },
  }));
  const ocmData     = {type: 'FeatureCollection', features: ocmFeatures};
  fs.writeFileSync('./public/assets/data/geojson/ocm.json', JSON.stringify(ocmData, undefined, 1));
  resources.push({name: 'Electric vehicle charging locations (OCM)', geojson: 'ocm.json'});

  // need the data to get the size to get the order to get the hue
  const resourcePromises = resources.map(async (resource) => {
    const data                  = require(`./public/assets/data/geojson/${resource.geojson}`);
    resource.dataFeaturesLength = data.features.length;
    return resource;
  });
// just returns resources, so no need to save the return value to a variable
  await Promise.all(resourcePromises);
  resources.sort((a, b) => a.dataFeaturesLength - b.dataFeaturesLength);
  resources.forEach((resource, r) => {
    const hue       = r / resources.length * 360;
    const layerType = resource.geojson.endsWith('polyline.json') ? 'line' :
                      resource.geojson.endsWith('region.json') ? 'fill' :
                      'circle';

    // for LayerControl
    resource.hue       = hue;
    resource.layerType = layerType;
  });

  fs.writeFileSync('./public/assets/data/resources.json', JSON.stringify(resources, undefined, 1));
})();
