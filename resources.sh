#!/bin/bash

## splash - android
# 9-patch file creation steps:
# export files from inkscape at the following sizes:
  # drawable-mdpi/.splash.png:     320 x  480
  # drawable-hdpi/.splash.png:     480 x  800
  # drawable-xhdpi/.splash.png:    720 x 1280
  # drawable-xxhdpi/.splash.png:   960 x 1600
  # drawable-xxxhdpi/.splash.png: 1280 x 1920
node 9patch.js
# npx cordova-res android --skip-config --copy --type splash --fit contain

## splash - ios
npx cordova-res ios --skip-config --copy --type splash --fit contain

## icons - android
# now done as per https://www.joshmorony.com/adding-icons-splash-screens-launch-images-to-capacitor-projects/
#npx cordova-res android --skip-config --copy --type icon --fit contain

## icons - ios
npx cordova-res ios --skip-config --copy --type icon --fit contain
