import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as Sentry from "@sentry/react";

Sentry.init({ dsn : "https://d9454ba6b3d5438fb81d4b9a4bb4fab6@o430655.ingest.sentry.io/5399197" });

ReactDOM.render(<App />, document.getElementById('root'));
