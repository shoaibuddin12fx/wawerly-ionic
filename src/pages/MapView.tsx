import React from 'react';
import Map from '../components/Map.js';
import {IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import {Location} from '../models/Location';
import {connect} from '../data/connect';
import * as selectors from '../data/selectors';
import './MapView.scss'

interface OwnProps {
}

interface StateProps {
  mapCenter: Location;
}

interface DispatchProps {
}

interface MapViewProps extends OwnProps, StateProps, DispatchProps {
}

const MapView: React.FC<MapViewProps> = ({mapCenter}) => {
  return (
    <IonPage id='map-view'>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot='start'>
            <IonMenuButton/>
          </IonButtons>
          <IonTitle>Map</IonTitle>
        </IonToolbar>
      </IonHeader>

    <IonContent class="map-page" forceOverscroll={false}>
      <Map mapCenter={mapCenter}/>
    </IonContent>
  </IonPage>
)};

export default connect<OwnProps, StateProps, DispatchProps>({
  mapStateToProps: (state) => ({
    mapCenter: selectors.mapCenter(state)
  }),
  component: MapView
});
