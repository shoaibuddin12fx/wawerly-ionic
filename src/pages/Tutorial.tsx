import React, { useState, useRef } from 'react';
import { IonContent, IonPage, IonHeader, IonToolbar, IonButtons, IonButton, IonSlides, IonSlide, IonIcon, useIonViewWillEnter } from '@ionic/react';
import { arrowForward } from 'ionicons/icons';
import { setMenuEnabled } from '../data/sessions/sessions.actions';
import { setHasSeenTutorial } from '../data/user/user.actions';
import './Tutorial.scss';
import { connect } from '../data/connect';
import { RouteComponentProps } from 'react-router';

interface OwnProps extends RouteComponentProps {}

interface DispatchProps {
  setHasSeenTutorial: typeof setHasSeenTutorial;
  setMenuEnabled: typeof setMenuEnabled;
}

interface TutorialProps extends OwnProps, DispatchProps { }

const Tutorial: React.FC<TutorialProps> = ({ history, setHasSeenTutorial, setMenuEnabled }) => {
  const [showSkip, setShowSkip] = useState(true);
  const slideRef = useRef<HTMLIonSlidesElement>(null);

  useIonViewWillEnter(() => {
    setMenuEnabled(false);
  });

  const startApp = async () => {
    await setHasSeenTutorial(true);
    await setMenuEnabled(true);
    history.push('/tabs/map', { direction: 'none' });
  };

  const handleSlideChangeStart = () => {
    slideRef.current!.isEnd().then(isEnd => setShowSkip(!isEnd));
  };

  return (
    <IonPage id="tutorial-page">
      <IonHeader no-border>
        <IonToolbar>
          <IonButtons slot="end">
            {showSkip && <IonButton color='primary' onClick={startApp}>Skip</IonButton>}
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen forceOverscroll={false}>

        <IonSlides ref={slideRef} onIonSlideWillChange={handleSlideChangeStart} pager={false}>
          <IonSlide>
            <img src="assets/img/tutorial/TUTORIAL-1.svg" alt="" className="slide-image" />
            <h2 className="slide-title">
              Welcome to <b>Waverley</b>
            </h2>
            <p>
              The <b>Waverley Transport app</b> is a preview of mobility-as-a-service in action, and a demonstration of sustainable transport.
            </p>
          </IonSlide>

          <IonSlide>
            <img src="assets/img/tutorial/TUTORIAL-2.svg" alt="" className="slide-image" />
            <h2 className="slide-title">What is MaaS?</h2>
            <p>
              <b>MaaS (Mobility as a Service)</b> is the concept that all travel options can be accessed through a single platform or app, allowing customers to have a seamless travel experience.
            </p>
          </IonSlide>

          <IonSlide>
            <img src="assets/img/tutorial/TUTORIAL-3.svg" alt="" className="slide-image" />
            <h2 className="slide-title">What is sustainable transport?</h2>
            <p>
              <b>Sustainable Transport</b> is affordable, operates fairly and efficiently, offers a choice of transport mode, and supports a competitive economy.
            </p>
          </IonSlide>

          <IonSlide>
            <img src="assets/img/tutorial/TUTORIAL-4.svg" alt="" className="slide-image" />
            <h2 className="slide-title">Ready to Ride?</h2>
            <IonButton fill="clear" onClick={startApp}>
              Continue
              <IonIcon slot="end" icon={arrowForward} />
            </IonButton>
          </IonSlide>
        </IonSlides>
      </IonContent>
    </IonPage>
  );
};

export default connect<OwnProps, {}, DispatchProps>({
  mapDispatchToProps: ({
    setHasSeenTutorial,
    setMenuEnabled
  }),
  component: Tutorial
});
