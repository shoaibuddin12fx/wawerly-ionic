import React, { useState } from 'react';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonDatetime,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonPopover,
  IonToolbar
} from '@ionic/react';
import './About.scss';
import { ellipsisHorizontal, ellipsisVertical } from 'ionicons/icons';
import AboutPopover from '../components/AboutPopover';

interface AboutProps { }

const About: React.FC<AboutProps> = () => {

  const [showPopover, setShowPopover] = useState(false);
  const [popoverEvent, setPopoverEvent] = useState();
  const [conferenceDate, setConferenceDate] = useState('2020-02-15T14:53:31+10:00');

  const presentPopover = (e: React.MouseEvent) => {
    setPopoverEvent(e.nativeEvent);
    setShowPopover(true);
  };

  return (
    <IonPage id="about-page">
      <IonContent>
        <IonHeader className="ion-no-border">
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonButtons slot="end">
              <IonButton onClick={presentPopover}>
                <IonIcon slot="icon-only" ios={ellipsisHorizontal} md={ellipsisVertical}></IonIcon>
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>

        <div className="about-header">
          {/* Instead of loading an image each time the select changes, use opacity to transition them */}
          <div className="about-image" style={{'opacity': '1'}}></div>
        </div>
        <div className="about-info">
          <h3 className="ion-padding-top ion-padding-start">About</h3>

          <p className="ion-padding-start ion-padding-end">
            The Waverley Transport app was developed in response to
            the <a href='https://opendata.transport.nsw.gov.au/waverley-transport-innovation-challenge'>Waverley
            Transport Innovation Challenge</a>.<br/>
            <br/>
            Our solution is a customer facing MaaS app (iOS, Android, and mobile web) that:
          </p>
          <ul>
            <li>encourages the use of designated safe pick-up/drop-off zones,</li>
            <li>enables multimodal trip planning that will increase transport options for customers,</li>
            <li>integrates both public transport and privately owned shared transport service providers,</li>
            <li>provides safe and continuous routes for prams, wheelchairs, and children on bicycles,</li>
            <li>provides a range of journey options for a defined trip that includes total journey route, cost, and
                time,
            </li>
            <li>calculates driving, cycling, and walking directions to the final destination including where a trip
                contains multiple segments and modes, and
            </li>
            <li>provides electric vehicle (EV) charging points within a journey plan.</li>
          </ul>

          <h3 className="ion-padding-top ion-padding-start">Details</h3>

          <IonList lines="none">
            <IonItem>
              <IonLabel>
                Us
              </IonLabel>
              <IonLabel className="ion-text-end">
                Smart Cities Transport
              </IonLabel>
            </IonItem>
            <IonItem detail href='https://linkedin.com/in/jayenashar'>
              <IonLabel>
                Lead Engineer
              </IonLabel>
              <IonLabel className="ion-text-end">
                Jayen Ashar
              </IonLabel>
            </IonItem>
            <IonItem>
              <IonLabel>
                Location
              </IonLabel>
              <IonLabel className="ion-text-end">
                Made with <span aria-label='love' role='img'>❤</span>️ in Sydney
              </IonLabel>
            </IonItem>
            <IonItem>
              <IonLabel>
                Date
              </IonLabel>
              <IonDatetime
                displayFormat="MMM DD, YYYY"
                max="2056"
                value={conferenceDate}
                onIonChange={(e) => setConferenceDate(e.detail.value as any)}>
              </IonDatetime>
            </IonItem>
          </IonList>

          <h3 className="ion-padding-top ion-padding-start">Legal</h3>

          <IonList lines="none">
            <IonItem detail
                     href='https://docs.google.com/document/d/1K3HFks1AwGEYBIg4UU_PTdsLRxocg_lYRriA5L0H5KU/edit?usp=sharing#bookmark=id.5srv0me8yqlj'
                     id='terms'>
              <IonLabel>
                Terms and Conditions
              </IonLabel>
            </IonItem>
            <IonItem detail
                     href='https://docs.google.com/document/d/1K3HFks1AwGEYBIg4UU_PTdsLRxocg_lYRriA5L0H5KU/edit?usp=sharing#bookmark=id.petv5i1qvp0y'
                     id='privacy-policy'>
              <IonLabel>
                Privacy Policy
              </IonLabel>
            </IonItem>
          </IonList>

        </div>
      </IonContent>

      <IonPopover
        isOpen={showPopover}
        event={popoverEvent}
        onDidDismiss={() => setShowPopover(false)}
      >
        <AboutPopover dismiss={() => setShowPopover(false)} />
      </IonPopover>
    </IonPage>
  );
};

export default React.memo(About);
