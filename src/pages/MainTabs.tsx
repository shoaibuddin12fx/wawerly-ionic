import React from 'react';
import {Redirect, Route} from 'react-router';
import SchedulePage from './SchedulePage';
import SpeakerList from './SpeakerList';
import SpeakerDetail from './SpeakerDetail';
import SessionDetail from './SessionDetail';
import MapView from './MapView';
import DirectionsList from './DirectionsList.js';
import DirectionsDetails from "./DirectionsDetails";
import About from './About';

interface MainTabsProps { }

const mainTabs: any = () => {
  return (
      [
        <Redirect exact path="/tabs" to="/tabs/map" />,
        // Using the render method prop cuts down the number of renders your components will have due to route changes.
        // Use the component prop when your component depends on the RouterComponentProps passed in automatically.
        <Route path="/tabs/schedule" render={() => <SchedulePage />} exact={true} />,
        <Route path="/tabs/speakers" render={() => <SpeakerList />} exact={true} />,
        <Route path="/tabs/speakers/:id" component={SpeakerDetail} exact={true} />,
        <Route path="/tabs/schedule/:id" component={SessionDetail} />,
        <Route path="/tabs/speakers/sessions/:id" component={SessionDetail} />,
        <Route path='/tabs/map' render={() => <MapView/>} exact={true}/>,
        <Route path='/tabs/map/directions-details' component={DirectionsDetails} />,
        <Route path='/tabs/map/directions/:params' component={DirectionsList} />,
        <Route path='/tabs/about' render={() => <About/>} exact={true}/>,
      ]
  );
};

export default mainTabs;
