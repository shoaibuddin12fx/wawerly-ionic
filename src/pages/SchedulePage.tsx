import React, {useCallback, useRef, useState} from 'react';

import {
  getConfig,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonMenuButton,
  IonModal,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonSearchbar,
  IonSegment,
  IonSegmentButton,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import {options, search} from 'ionicons/icons';


import * as favourites from "../util/favourites";
import StopList from "../components/StopList";
import StopListFilter from "../components/StopListFilter";
import {connect} from '../data/connect';
import {getGeolocation} from "../util/geolocate";
import {get, init, set} from "../util/storage";


import './SchedulePage.scss'

interface OwnProps {
}

interface StateProps {
  mode: 'ios' | 'md'
}

interface DispatchProps {
}

type SchedulePageProps = OwnProps & StateProps & DispatchProps;

type StopListFilter = {
  modes: string[],
  radius: number,
};

const allModes = ['bus', 'ferry', 'train', 'tram', 'metro'];

init('stop-list-filter', {
  modes: allModes,
  radius: 1000,
});

const SchedulePage: React.FC<SchedulePageProps> = ({mode}) => {
  const [segment, setSegment] = useState<'nearest' | 'favourites'>('nearest');
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const [filter, setFilter] = useState<StopListFilter>(get('stop-list-filter'));
  const saveFilter = (filter: StopListFilter) => setFilter(set('stop-list-filter', filter));
  const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  const ionRefresherCallback = useCallback(element => {
    if (element) {
      // @ts-ignore
      ionRefresherRef.current = element;
      // ionic doesn't have a way to make a refresher look like it's refreshing, so we hack it
      ionRefresherRef.current!.classList.add("refresher-active", "refresher-refreshing");
    }
  }, []);
  const [isRefreshing, setIsRefreshing] = React.useState(false);
  const [geolocation, setGeolocation] = useState<number[] | null>(null);
  const [locations, setLocations] = useState(null);
  const [departures, setDepartures] = useState({});
  const [searchText, setSearchText] = useState('');

  const pageRef = useRef<HTMLElement>(null);

  const ios = mode === 'ios';

  React.useEffect(() => {
    if (process.env.NODE_ENV === 'development')
      setGeolocation([151.0860997, -33.7745328]);
    else
      getGeolocation().then(setGeolocation);
  }, []);

  React.useEffect(() => {
    if (geolocation?.length === 2) {
      (async function () {
        if (process.env.NODE_ENV === 'development') {
          // @ts-ignore
          setLocations({...(await import('../models/Locations')).default});
        } else {
          const url = `https://api.tripgo.com/v1/locations.json?lng=${geolocation[0]}&lat=${geolocation[1]}&radius=${filter.radius}&modes=pt_pub`;
          const response = await fetch(url,
            {
              headers: {
                'X-TripGo-Key': '44b87680b7a8a3448244aa0d04920def',
              },
            });
          setLocations(await response.json());
        }
      })();
    }
  }, [filter.radius, geolocation]);

  React.useEffect(() => {
    // @ts-ignore
    const stops = [].concat.apply([], locations?.groups?.map(group => group?.stops || []) || []);
    (async function () {
      if (stops.length) {
        if (process.env.NODE_ENV === 'development') {
          setDepartures((await import('../models/Departures')).default);
        } else {
          const url = `https://api.tripgo.com/v1/departures.json`;
          //   1 stops -> 3.084 seconds
          //   2 stops -> 2.656 seconds
          //   5 stops -> 2.6 seconds
          //  10 stops -> 5 seconds
          //  20 stops -> 3.777 seconds
          //  50 stops -> 4.665 seconds
          // 180 stops -> 8.309 seconds
          // @ts-ignore
          const embarkationStops = stops.map(stop => stop.code);
          embarkationStops.push(...favourites.toList());
          const response = await fetch(url,
            {
              body: JSON.stringify({
                "region": "AU_NSW_Sydney",
                "embarkationStops": embarkationStops
              }),
              headers: {
                'Content-type': 'application/json',
                'X-TripGo-Key': '44b87680b7a8a3448244aa0d04920def',
              },
              method: 'POST',
            });
          setDepartures(await response.json());
        }
      }
      if (isRefreshing)
        await ionRefresherRef.current!.complete();
      // not really necessary because after the first refresh, isRefreshing will always be true when you get here
      setIsRefreshing(false);
      // useEffect runs on mount when locations is null, so we don't want to mark it as done refreshing
      if (locations)
        ionRefresherRef.current!.classList.remove("refresher-active", "refresher-refreshing")
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [locations]);

  const doRefresh = async () => {
    setIsRefreshing(true);
    setGeolocation(await getGeolocation());
  };

  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          {!showSearchbar &&
          <IonButtons slot="start">
            <IonMenuButton/>
            </IonButtons>
          }
          {ios &&
            <IonSegment value={segment} onIonChange={(e) => setSegment(e.detail.value as any)}>
              <IonSegmentButton value="nearest">
                Nearest
              </IonSegmentButton>
              <IonSegmentButton value="favourites">
                Favourites
              </IonSegmentButton>
            </IonSegment>
          }
          {!ios && !showSearchbar &&
            <IonTitle>Stops</IonTitle>
          }
          {showSearchbar &&
            <IonSearchbar showCancelButton="always" placeholder="Search" onIonChange={(e: CustomEvent) => setSearchText(e.detail.value)} onIonCancel={() => setShowSearchbar(false)}/>
          }

          <IonButtons slot="end">
            {!ios && !showSearchbar &&
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}/>
              </IonButton>
            }
            {!showSearchbar &&
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === 'ios' ? 'Filter' : <IonIcon icon={options} slot="icon-only" />}
              </IonButton>
            }
          </IonButtons>
        </IonToolbar>

        {!ios &&
          <IonToolbar>
            <IonSegment value={segment} onIonChange={(e) => setSegment(e.detail.value as any)}>
              <IonSegmentButton value="nearest">
                Nearest
              </IonSegmentButton>
              <IonSegmentButton value="favourites">
                Favourites
              </IonSegmentButton>
            </IonSegment>
          </IonToolbar>
        }
      </IonHeader>

      {/*z-index: 9 to be below the header*/}
      <IonContent fullscreen={true} style={{zIndex: 9}}>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Stops</IonTitle>
          </IonToolbar>
          <IonToolbar>
            <IonSearchbar placeholder="Search" onIonChange={(e: CustomEvent) => setSearchText(e.detail.value)}/>
          </IonToolbar>
        </IonHeader>

        <IonRefresher slot="fixed" ref={ionRefresherCallback} onIonRefresh={doRefresh}>
          <IonRefresherContent/>
        </IonRefresher>


        <StopList departures={departures}
                  filter={filter}
                  geolocation={geolocation}
                  searchText={searchText}
                  stopCodes={segment === 'nearest' ? null : favourites.toList()}/>
        {filter.modes.length < allModes.length && <IonItem><IonLabel>
          Modes filtered out: {allModes.filter(mode => !filter.modes.includes(mode)).join(', ')}
        </IonLabel></IonItem>}
        <IonItem>
          <IonLabel>
            Powered by SkedGo/TripGo
          </IonLabel>
        </IonItem>
      </IonContent>

      <IonModal
        isOpen={showFilterModal}
        onDidDismiss={() => setShowFilterModal(false)}
        swipeToClose={true}
        presentingElement={pageRef.current!}
        cssClass="session-list-filter"
      >
        <StopListFilter
          onDismissModal={() => setShowFilterModal(false)}
          setFilter={saveFilter}
          {...filter}
        />
      </IonModal>
    </IonPage>
  );
};

export default connect<OwnProps, StateProps, DispatchProps>({
  mapStateToProps: () => ({
    mode: getConfig()!.get('mode')
  }),
  component: React.memo(SchedulePage)
});
