import React from 'react';
import {
  getConfig,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonNote,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import {flagOutline, flagSharp} from 'ionicons/icons';
import {toGeoJSON} from '@mapbox/polyline';
import {useLocation} from 'react-router';

import JynBackButton from '../components/JynBackButton';
import Map from '../components/Map';
import debounce from '../util/debounce';
import mapboxgl from '../util/mapbox-gl';
import {ResultGroup} from '../components/ResultGroup';
import {Segments} from '../components/Segments';
import {mapCenter} from '../data/selectors';
import {sineScroll} from '../util/scroll';

import './DirectionsDetails.scss';
import {getStringFromSeconds, hourMinuteSecondFormat} from '../util/time';
import {getIcon} from '../components/Icon';

const INSTRUCTIONS = {
  UNKNOWN:             '', //No turn instruction available
  HEAD_TOWARDS:        'Head towards', //Start the segment heading over the street
  CONTINUE_STRAIGHT:   'Continue straight', //Continue forward (+/- 20 degrees)
  TURN_SLIGHTLY_LEFT:  'Turn slightly left', //Turn left between 20 and 60 degrees
  TURN_LEFT:           'Turn left', //Turn left between 60 and 115 degrees
  TURN_SHARPLY_LEFT:   'Turn sharply left', //Turn left more than 115 degrees
  TURN_SLIGHTLY_RIGHT: 'Turn slightly right', //Turn right between 20 and 60 degrees
  TURN_RIGHT:          'Turn right', //Turn right between 60 and 115 degrees
  TURN_SHARPLY_RIGHT:  'Turn sharply right', //Turn right more than 115 degrees
};

const UA                            = navigator.userAgent;
const isNotPropagatingPointerEvents = /\b(AppleWebKit|Chrome)\/\b/.test(UA);

function getSegmentDetails(segment, s) {
  if (segment.realtimeStops) {
    segment.stopPredictions = {};
    segment.realtimeStops.forEach(stop => segment.stopPredictions[stop.code] = stop);
  }

  const isExpandable = segment.template.streets?.length > 1 || segment.template.shapes;
  const Expandable   = isExpandable ? 'details' : 'div';

  return <IonItem key={s}>
    <IonLabel className='segment-main'>
      <div className='segment-summary'>
        {getIcon(segment?.template?.modeInfo)}&nbsp;
        {hourMinuteSecondFormat.format(new Date(1000 * segment?.startTime))}
      </div>
      <div>
        {segment?.template?.from?.address ?? segment?.template?.mini?.instruction}
        <Expandable className='segment-details' open={process.env.NODE_ENV === 'development'}>
          <summary>
            {isExpandable && (segment.stops ? `${segment.stops} stops` : 'Directions')}
          </summary>
          <table>
            <tbody>{segment.template.streets?.length > 1 && segment.template.streets.map((street, s) =>
              // TODO: dismount=false > cycle
              // TODO: dismount=true > walk
              <tr key={s}>
                <td>{INSTRUCTIONS[street?.instruction]}</td>
                <td>{street?.name}</td>
              </tr>,
            )}</tbody>
          </table>
          {segment.template.shapes?.filter(shape => shape.travelled)
                  .map(shape => shape.stops.map((stop, s) => {
                    const departure = segment.stopPredictions?.[stop.code].predictedDeparture ??
                                      segment.startTime + stop.relativeDeparture;
                    const arrival   = segment.stopPredictions?.[stop.code].predictedArrival ??
                                      segment.startTime + stop.relativeArrival;
                    const date      = new Date(1000 * (s === shape.stops.length - 1 ? arrival : departure));
                    return <div key={s}>{hourMinuteSecondFormat.format(date)} {stop.name}</div>;
                  }))}
        </Expandable>
      </div>
    </IonLabel>
    <IonNote slot='end'>
      {getStringFromSeconds(segment?.endTime - segment?.startTime)}&nbsp;
    </IonNote>
  </IonItem>;
}

export default () => {
  const location = useLocation();
  const ios      = getConfig().get('mode') === 'ios';
  const state    = location.state;

  const mapBackgroundRef                  = React.useRef();
  const scrollableRef                     = React.useRef();
  const [trip, setTrip]                   = React.useState(state?.trip);
  const [directions, setDirections]       = React.useState();
  const [bounds, setBounds]               = React.useState();
  const [halfScreenMap, setHalfScreenMap] = React.useState(false);

  React.useEffect(() => {
    tryScroll();

    function tryScroll() {
      /** @type HTMLDivElement */
      const mapBackgroundElement = mapBackgroundRef.current;
      /** @type HTMLDivElement */
      const scrollableElement    = scrollableRef.current;
      if (mapBackgroundElement && scrollableElement) {
        const height = mapBackgroundElement.getBoundingClientRect().height;
        if (height !== 0) {
          sineScroll(scrollableElement, 'scrollTop', height / 2);
          setHalfScreenMap(true);

          // TODO: move to onScroll={debounce(handler, 200)}
          const handler = () => {
            const scrollTop = scrollableElement.scrollTop;
            const height    = mapBackgroundElement.getBoundingClientRect().height;
            if (scrollTop < height / 4) {
              sineScroll(scrollableElement, 'scrollTop', 0);
              scrollableElement.classList.add('full-map');
              setHalfScreenMap(false);
            } else {
              if (scrollTop < height * 3 / 4) {
                sineScroll(scrollableElement, 'scrollTop', height / 2);
                setHalfScreenMap(true);
              } else if (scrollTop < height) {
                sineScroll(scrollableElement, 'scrollTop', height);
              }
              scrollableElement.classList.remove('full-map');
            }
          };
          scrollableElement.addEventListener('scroll', debounce(handler, 200));
          return;
        }
      }
      setTimeout(tryScroll, 100);
    }
  }, []);

  React.useEffect(() => {
    if (state?.trip) {
      const trip = state.trip;
      setTrip(trip);
      const directions = {
        "type":     "FeatureCollection",
        "features": trip.segments.filter(segment => {
          // template could be stationary segment
          const template = segment.template;
          return (template.streets || template.shapes) && template.modeInfo?.color;
        }).flatMap(segment => {
          const template = segment.template;
          const ss       = template.streets || template.shapes;
          const color    = segment.serviceColor ?? template.modeInfo.color;
          const rgb      = `#` +
                           color.red.toString(16).padStart(2, '0') +
                           color.green.toString(16).padStart(2, '0') +
                           color.blue.toString(16).padStart(2, '0');
          return ss.map(s => ({
            type:       'Feature',
            geometry:   toGeoJSON(s.encodedWaypoints),
            properties: {rgb: rgb, opacity: template.streets || s.travelled ? 1 : 0.2},
          }));
        }),
      };

      const coordinates = directions.features.filter(feature => feature.properties.opacity === 1.0)
                                    .flatMap(feature => feature.geometry.coordinates);
      const bounds      = coordinates.reduce((bounds, coord) => bounds.extend(coord),
        new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

      setDirections(directions);
      setBounds(bounds.toArray());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state?.trip])

  const onCoverClick   = () => {
    /** @type HTMLDivElement */
    const scrollableElement = scrollableRef.current;
    sineScroll(scrollableElement, 'scrollTop', 0);
    scrollableElement.classList.add('full-map');
    setHalfScreenMap(false);
  };
  const onSummaryClick = () => {
    /** @type HTMLDivElement */
    const mapBackgroundElement = mapBackgroundRef.current;
    /** @type HTMLDivElement */
    const scrollableElement    = scrollableRef.current;
    const scrollTop            = scrollableElement.scrollTop;
    const height               = mapBackgroundElement.getBoundingClientRect().height;
    if (scrollTop < height * 3 / 4) {
      sineScroll(scrollableElement, 'scrollTop', height);
      scrollableElement.classList.remove('full-map');
    } else {
      sineScroll(scrollableElement, 'scrollTop', 0);
      scrollableElement.classList.add('full-map');
      setHalfScreenMap(false);
    }
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // chrome doesn't propagate pointer events up to the scrollable parent, so we scroll manually
  let clientY;
  const onTouchStart = e => clientY = e.touches[0].clientY;
  const onTouchMove  = e => {
    /** @type HTMLDivElement */
    const scrollable = scrollableRef.current;
    if (isNotPropagatingPointerEvents && scrollable.classList.contains('full-map')) {
      const newClientY = e.changedTouches[0].clientY;
      const deltaY     = newClientY - clientY;
      clientY          = newClientY;
      scrollable.scrollTop -= deltaY;
    }
  };
  const onWheel      = e => {
    /** @type HTMLDivElement */
    const scrollable = scrollableRef.current;
    if (isNotPropagatingPointerEvents && scrollable.classList.contains('full-map')) {
      scrollable.scrollTop += e.deltaY;
    }
  };
  // chrome doesn't propagate pointer events up to the scrollable parent, so we scroll manually
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  return (
    <IonPage>
      {ios && <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <JynBackButton defaultHref='/tabs/map'/>
          </IonButtons>
        </IonToolbar>
      </IonHeader>}
      <IonContent>
        <div className='map-background' ref={mapBackgroundRef} slot='fixed'>
          <Map bounds={bounds} directions={directions} mapCenter={mapCenter()} zoomOutTop={halfScreenMap}/>
        </div>
        <div className='outer-scrollable' ref={scrollableRef} slot='fixed'>
          <div className='map-cover' onClick={onCoverClick}/>
          {!ios && <JynBackButton defaultHref='/tabs/map' slot='fixed'/>}
          <IonList className='directions-details'
                   onTouchStart={onTouchStart}
                   onTouchMove={onTouchMove}
                   onWheel={onWheel}>
            <IonItem className='directions-details-summary' onClick={onSummaryClick}>
              <ResultGroup trip={trip}>
                <Segments trip={trip}/>
              </ResultGroup>
            </IonItem>
            {trip?.segments?.map(getSegmentDetails)}
            <IonItem>
              <IonLabel className='segment-main'>
                <div className='segment-summary'>
                  <IonIcon ios={flagOutline} md={flagSharp}/>&nbsp;
                  {hourMinuteSecondFormat.format(new Date(1000 * trip.arrive))}
                </div>
                <div className='segment-details'>
                  {trip.segments[trip.segments.length - 1].template.to.address}
                </div>
              </IonLabel>
            </IonItem>
          </IonList>
        </div>
      </IonContent>
    </IonPage>
  );
};
