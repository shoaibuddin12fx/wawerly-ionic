import {
  IonButton,
  IonButtons,
  IonChip,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import {chevronDown} from 'ionicons/icons';
import React, {useRef} from 'react';
import moment from 'moment-timezone';
import {useHistory, withRouter} from 'react-router';

import Geocoder, {YOUR_LOCATION} from '../util/Geocoder';
import JynBackButton from '../components/JynBackButton';
import {CacheContext} from '../util/contexts';
import {ResultGroup} from '../components/ResultGroup';
import {Segments, unhash} from '../components/Segments';
import {TimeOptions} from '../components/TimeOptions';
import {TransportOptions} from "../components/TransportOptions";
import {accessToken} from '../util/mapbox-gl';
import {get, init, set} from '../util/storage';
import {getGeolocation, hasGeolocatePermission} from "../util/geolocate";
import {getStringFromSeconds, hourMinuteFormat} from '../util/time';
import {getModeIcon} from '../components/Icon';

import './DirectionsList.scss';

const timeAnchorsLabel = {
  departAfter:  'Depart at',
  arriveBefore: 'Arrive by',
};

init('transport-options', {
  /** @type string[] */
  preferredModes:  [],
  route:           'best',
  /** @type string[] */
  connectingModes: [],
});
init('directions-modes-to-hide', []);

const getParam = function (location) {
  const centre = location.centre;
  const input  = location.input;
  let param    = `(${centre[1]},${centre[0]})`;
  if (input !== YOUR_LOCATION) {
    param += `"${input}`;
  }
  return param;
};

const DirectionsList = ({match}) => {
  const urlSearchParams = new URLSearchParams(`?${match.params['params']}`);

  const pageRef = React.useRef();

  const cache = React.useContext(CacheContext);

  const [from, setFrom]                                 = React.useState(cache.directionsFrom || {});
  const cacheFrom                                       = from => {
    setFrom(from);
    if (from.input === YOUR_LOCATION) {
      const {centre, ...rest} = from;
      cache.directionsFrom    = {...rest};
    } else {
      cache.directionsFrom = from;
    }
  };
  const [fromGeocoder, setFromGeocoder]                 = React.useState(null);
  const [to, setTo]                                     = React.useState({
    input:  urlSearchParams.get('toInput'),
    centre: urlSearchParams.get('toCentre').split(',').map(Number),
  });
  const [toGeocoder, setToGeocoder]                     = React.useState(null);
  const [showTimeOptions, setShowTimeOptions]           = React.useState(false);
  const [timeOptions, setTimeOptions]                   = React.useState(cache.directionsTime || {
    anchor: 'departAfter',
    date:   null, // yyyy-mm-dd
    time:   null, // hh:mm
  });
  const cacheTimeOptions                                =
          timeOptions => setTimeOptions(cache.directionsTime = timeOptions);
  const [showTransportOptions, setShowTransportOptions] = React.useState(false);
  const [transportOptions, setTransportOptions]         = React.useState(get('transport-options'));
  const saveTransportOptions                            =
          transportOptions => setTransportOptions(set('transport-options', transportOptions));
  // TODO: switch to useReducer
  const [results, setResults]                           = React.useState({});
  // TODO: switch to useReducer
  const [modesToHide, setModesToHide]                   = React.useState(get('directions-modes-to-hide'));
  const saveModesToHide                                 = getModesToHide =>
    setModesToHide(oldModesToHide => set('directions-modes-to-hide', getModesToHide(oldModesToHide)));
  const ionRefresherRef                                 = useRef(null);
  const history                                         = useHistory();

  const getGeocoderCallback = (state, setState, setGeocoder) => element => {
    if (element) {
      const geocoder = new Geocoder({
        accessToken,
        localGeocoder,
        minLength:   0,
        placeholder: 'Choose starting point',
      });
      geocoder.addTo(`#${element.id}`); // does not return geocoder
      if (state.input) {
        // Set input value to passed value and clear everything else.
        geocoder._inputEl.value = state.input;
        geocoder._typeahead.selected = null;
        geocoder._typeahead.clear();
      } else {
        if (hasGeolocatePermission()) {
          geocoder._inputEl.value = YOUR_LOCATION;
        }
      }
      geocoder.on('result', (result) => {
        if (result.result.geolocate) {
          getGeolocation().then(centre => setState({input: result.result.place_name, centre}));
        } else {
          setState({input: result.result.place_name, centre: result.result.center});
        }
      });
      setGeocoder(geocoder);

      function localGeocoder(query) {
        if (YOUR_LOCATION.toLowerCase().includes(query.toLowerCase())) {
          return [{
            type:       'Feature',
            id:         '0',
            place_name: YOUR_LOCATION,
            properties: {},
            geometry:   null,
            geolocate:  true,
          }];
        }
      }
    }
  };
  const fromLocation        = React.useCallback(getGeocoderCallback(from, cacheFrom, setFromGeocoder), []);
  const toLocation          = React.useCallback(getGeocoderCallback(to, setTo, setToGeocoder), []);

  React.useEffect(() => {
    if (hasGeolocatePermission() && (!from.input || from.input === YOUR_LOCATION)) {
      getGeolocation().then(centre => cacheFrom({input: YOUR_LOCATION, centre}));
    }
    if (process.env.NODE_ENV === "development") {
      import('../models/Results').then(results => setResults(results.results));
    }
    // eslint-disable-next-line
  }, []);

  React.useEffect(() => {
    if (toGeocoder && from.centre) {
      toGeocoder.setProximity({longitude: from.centre[0], latitude: from.centre[1]});
    }
  }, [toGeocoder, from.centre]);
  React.useEffect(() => {
    if (fromGeocoder && to.centre) {
      fromGeocoder.setProximity({longitude: to.centre[0], latitude: to.centre[1]});
    }
  }, [fromGeocoder, to.centre]);

  const doFetch = isRefresh => () => {
    if (process.env.NODE_ENV === "development") {
      console.log(`${Date.now()}\tfrom, to`, JSON.stringify(from), JSON.stringify(to));
    }
    if (from.centre && to.centre) {
      const fromParam = getParam(from);
      const toParam   = getParam(to);
      setResults({});

      const globeModes = ['cy_bic', 'pt_pub'];
      if (transportOptions.connectingModes.includes('drive')) {
        globeModes.push('me_car');
      }
      if (transportOptions.connectingModes.includes('ride')) {
        globeModes.push('ps_tax');
      }

      const fetches = [];
      if (process.env.NODE_ENV === "development") {
        fetches.push(fetchForMode('globe', globeModes));
      } else {
        fetches.push(fetchForMode('publicTransport', ['pt_pub']));
        fetches.push(fetchForMode('walk', ['wa_wal']));
        fetches.push(fetchForMode('bicycle', ['cy_bic']));
        fetches.push(fetchForMode('globe', globeModes));
        fetches.push(fetchForMode('taxi', ['ps_tax']));
      }
      if (isRefresh) {
        Promise.all(fetches).then(() => ionRefresherRef.current.complete());
      }

      async function fetchForMode(id, modes) {
        const modeParams    = modes.map(mode => `modes=${mode}`).join('&');
        const unix          = timeOptions.date && timeOptions.time ?
                              moment.tz(`${timeOptions.date} ${timeOptions.time}`, 'Australia/Sydney').unix() :
                              moment().unix();
        const ptPubModes    = ['bus', 'metro', 'train', 'tram', 'ferry'];
        let avoidModesParam = '';
        if (transportOptions.preferredModes.length) {
          const avoidModesParams = ptPubModes.filter(ptPubMode => !transportOptions.preferredModes.includes(ptPubMode))
                                             .map(ptPubMode => `avoidModes=pt_pub_${ptPubMode}`);
          avoidModesParam        = `&${avoidModesParams.join('&')}`
        }
        const routeParam   = transportOptions.route === 'fewer' ? '&wp=(0,0,0,2)' :
                             transportOptions.route === 'less' ? '&wp=(0,0,2,0)&ws=0' :
                             transportOptions.route === 'wheelchair' ? '&wheelchair=true' :
                             '';
        const url          = `https://api.tripgo.com/v1/routing.json?from=${fromParam}&${timeOptions.anchor}=${unix}&to=${toParam}&${modeParams}&v=11&ir=1&includeStops=true${avoidModesParam}${routeParam}`;
        const response     = await fetch(url,
          {
            headers: {
              'X-TripGo-Key': '44b87680b7a8a3448244aa0d04920def',
            },
          });
        const responseJSON = await response.json();
        setResults(prevState => ({...prevState, [id]: responseJSON}));
      }
    }
  };
  React.useEffect(doFetch(false), [from, to, timeOptions, transportOptions]);

  for (const mode in results) {
    const result = results[mode];
    const groups = result.groups;
    if (groups && groups.length) {
      for (const group of groups) {
        const trips = group.trips;
        trips.sort((a, b) => a.weightedScore - b.weightedScore);
        group.score    = trips[0].weightedScore;
        group.duration = trips[0].arrive - trips[0].depart;
      }
      groups.sort((a, b) => a.score - b.score);
      result.score    = groups[0].score;
      result.duration = groups[0].duration;
    }
  }
  const modes = Object.keys(results).filter(mode => results[mode].score);
  modes.sort((a, b) => a === 'car' ? 1 : b === 'car' ? -1 : results[a].score - results[b].score);

  const groupModes = [];
  for (const mode in results) {
    const result = results[mode];
    const groups = result.groups;
    if (groups) {
      groupModes.push(...groups.map(group => ({group, mode})));
    }
  }
  groupModes.sort((a, b) => a.mode === 'car' ? 1 : b.mode === 'car' ? -1 : a.group.score - b.group.score);

  // These are just used for display, so it's ok to display them in local time, but let's use Sydney time anyway
  const today      = moment().tz('Australia/Sydney').format(moment.HTML5_FMT.DATE);
  const chosenDate = timeOptions.date && timeOptions.time ?
                     moment.tz(`${timeOptions.date} ${timeOptions.time}`,
                       'Australia/Sydney').toDate() :
                     new Date();
  const time       = hourMinuteFormat.format(chosenDate);
  const datetime   = chosenDate.toLocaleString(undefined,
    {
      weekday:  'short',
      month:    'short',
      day:      'numeric',
      hour:     'numeric',
      minute:   'numeric',
      timeZone: 'Australia/Sydney',
    });

  return (
    <IonPage id='speaker-list' ref={pageRef}>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot='start'>
            <JynBackButton defaultHref='/tabs/map'/>
          </IonButtons>
          <IonTitle>Directions</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent className={`outer-content`}>
        <IonRefresher slot="fixed" ref={ionRefresherRef} onIonRefresh={doFetch(true)}>
          <IonRefresherContent/>
        </IonRefresher>

        <div id='from-location' className='geocoder from-location' ref={fromLocation}/>
        <div id='to-location' className='geocoder to-location' ref={toLocation}/>
        <div className='mode-durations'>
          {modes.map(mode => {
            const modeIcon = getModeIcon(mode);
            const visible  = !modesToHide.includes(mode);
            const onClick  = () => {
              saveModesToHide(modesToHide => modesToHide.includes(mode) ?
                                             modesToHide.filter(modeToHide => modeToHide !== mode) :
                                             [...modesToHide, mode]);
            };

            return <IonChip color='primary' onClick={onClick} outline={!visible} key={mode}>
              {modeIcon} <IonLabel>{getStringFromSeconds(results[mode].duration)}</IonLabel>
            </IonChip>;
          })}
        </div>
        <div className='query-options'>
          {showTimeOptions &&
           <TimeOptions close={() => setShowTimeOptions(false)}
                        {...timeOptions}
                        presentingElement={pageRef.current}
                        setOptions={cacheTimeOptions}/>
          }
          <IonButton fill='clear' onClick={() => setShowTimeOptions(true)}>
            {timeAnchorsLabel[timeOptions.anchor]}
            {!timeOptions.date || timeOptions.date === today ?
             ` ${time}` :
             ` ${datetime.replace(/ /g, "\u00a0")}`} <IonIcon icon={chevronDown}/>
          </IonButton>
          {showTransportOptions &&
           <TransportOptions close={() => setShowTransportOptions(false)}
                             {...transportOptions}
                             presentingElement={pageRef.current}
                             setOptions={saveTransportOptions}/>
          }
          <IonButton fill='clear' onClick={() => setShowTransportOptions(true)}>Options</IonButton>
        </div>
        <IonList>
          {groupModes.filter(({mode}) => !modesToHide.includes(mode)).map(({group, mode}, gm) => {
            const trip             = group.trips[0];
            const segmentTemplates = results[mode].segmentTemplates;
            unhash(trip, segmentTemplates);
            return <IonItem detail
                            key={gm}
                            onClick={() => history.push('/tabs/map/directions-details', {trip})}>
              <ResultGroup trip={trip}>
                <Segments trip={trip}/>
              </ResultGroup>
            </IonItem>;
          })}
          <IonItem>
            <IonLabel>
              Powered by SkedGo/TripGo
            </IonLabel>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default withRouter(DirectionsList);
