import React from 'react';
import {
  IonIcon,
  IonItem,
  IonItemDivider,
  IonItemGroup,
  IonItemSliding,
  IonLabel,
  IonList,
  IonListHeader,
} from '@ionic/react';
import moment from 'moment-timezone';
import {heart, heartOutline, pulse, remove} from 'ionicons/icons';
import LatLon from 'geodesy/latlon-ellipsoidal-vincenty.js';

import * as favourites from "../util/favourites";
import {hourMinuteSecondFormat} from '../util/time';

const getStopCodesStop = function (stops) {
  const stopCodesStop   = {};
  const addStopCodeStop = stop => {
    stopCodesStop[stop.stopCode] = stop;
    // eslint-disable-next-line no-unused-expressions
    stop.children?.forEach(addStopCodeStop);
  };
  stops.forEach(addStopCodeStop);
  return stopCodesStop;
};

let getNumberDirectionsServices = function (services) {
  const numberDirectionsServices = {};
  services.forEach(service => {
    const numberDirection                     = JSON.stringify([service.serviceNumber,
                                                                service.serviceDirection]);
    numberDirectionsServices[numberDirection] = numberDirectionsServices[numberDirection] || [];
    numberDirectionsServices[numberDirection].push(service);
  });
  return numberDirectionsServices;
};
export default ({departures, filter, geolocation, searchText, stopCodes}) => {
  const embarkationStops = departures?.embarkationStops;
  if (geolocation && embarkationStops?.length) {
    const geolocationLatLon       = new LatLon(geolocation[1], geolocation[0]);
    const stopCodesStop           = getStopCodesStop(departures.stops);
    const visibleEmbarkationStops = embarkationStops.filter(embarkationStop =>
      stopCodes === null || stopCodes.includes(stopCodesStop[embarkationStop.stopCode].code),
    ).filter(embarkationStop => {
      if (stopCodesStop[embarkationStop.stopCode].name.toLowerCase().includes(searchText.toLowerCase())) {
        embarkationStop.visibleServices = embarkationStop.services;
      } else {
        embarkationStop.visibleServices =
          embarkationStop.services.filter(service =>
            service.searchString.toLowerCase().includes(searchText.toLowerCase()));
      }
      embarkationStop.visibleServices =
        embarkationStop.visibleServices.filter(service => filter.modes.includes(service.mode));
      return embarkationStop.visibleServices.length;
    }).filter(embarkationStop => {
      const stopCode         = embarkationStop.stopCode;
      const stop             = stopCodesStop[stopCode];
      const stopLatLon       = new LatLon(stop.lat, stop.lng);
      embarkationStop.metres = stopLatLon.distanceTo(geolocationLatLon);
      return embarkationStop.metres <= filter.radius || process.env.NODE_ENV === 'development';
    });

    if (visibleEmbarkationStops.length) {
      visibleEmbarkationStops.sort((a, b) => a.metres - b.metres);

      return <IonList>
        {visibleEmbarkationStops.map(embarkationStop =>
          <Stop embarkationStop={embarkationStop} key={embarkationStop.stopCode}/>,
        )}
      </IonList>;

      function Stop({embarkationStop}) {
        const stopCode                 = embarkationStop.stopCode;
        const stop                     = stopCodesStop[stopCode];
        const numberDirectionsServices = getNumberDirectionsServices(embarkationStop.visibleServices);

        const [isFavourite, setIsFavourite] = React.useState(favourites.includes(stop.code));

        const toggleFavourite = () => {
          setIsFavourite(isFavourite ?
                         favourites.remove(stop.code) :
                         favourites.add(stop.code));
        };

        return <IonItemGroup>
          <IonItemDivider onClick={toggleFavourite} sticky>
            <IonLabel>
              {stop.name} &mdash;&nbsp;
              {embarkationStop.metres.toFixed(0)}m away
            </IonLabel>
            <IonIcon icon={isFavourite ? heart : heartOutline} slot='end' style={{paddingRight: '10px'}}/>
          </IonItemDivider>
          {Object.entries(numberDirectionsServices).map(([numberDirection, services]) => {
            const [number, direction] = JSON.parse(numberDirection);
            const style               = services[0]?.serviceColor ?
                                        {
                                          paddingLeft: '10px',
                                          borderLeft:  `2px solid rgb(${services[0].serviceColor.red},${services[0].serviceColor.green},${services[0].serviceColor.blue})`,
                                        } :
                                        {};
            return (
              <IonItemSliding key={numberDirection}>
                <IonItem>
                  <IonLabel style={style}>
                    <h3>
                      {number} &mdash;&nbsp;
                      {direction}
                    </h3>
                    {services.map(service => {
                      const time = moment.unix(service.realTimeDeparture ?? service.startTime).tz('Australia/Sydney');
                      return (
                        <p key={service.startTime}>
                          {hourMinuteSecondFormat.format(time.toDate())}&nbsp;
                          <IonIcon icon={service.realTimeStatus === "IS_REAL_TIME" ? pulse : remove}/>&nbsp;
                          {time.fromNow()}
                        </p>
                      )
                    })}
                  </IonLabel>
                </IonItem>
              </IonItemSliding>
            );
          })}
        </IonItemGroup>;
      }
    }
  }
  return (
    <IonList>
      <IonListHeader>
        No Stops Found
      </IonListHeader>
    </IonList>
  );
};
