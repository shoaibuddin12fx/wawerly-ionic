import React, {useState} from 'react';
import {IonButton, IonFooter, IonHeader, IonModal, IonSegment, IonSegmentButton, IonToolbar} from '@ionic/react';
import moment from 'moment';

/**
 * @type React.FC
 */
export const TimeOptions = ({presentingElement, ...props}) => {
  const [anchor, setAnchor] = useState(props.anchor);
  const [date, setDate]     = useState(props.date);
  const [time, setTime]     = useState(props.time);

  const now   = moment().format(moment.HTML5_FMT.TIME);
  const today = moment().format(moment.HTML5_FMT.DATE);

  const onTimeChange = e => {
    setTime(e.target.value);
    if (!date) {
      setDate(today);
    }
  };
  const onDateChange = e => {
    setDate(e.target.value);
    if (!time) {
      setTime(now);
    }
  };
  const onReset      = () => {
    setDate(null);
    setTime(null);
  };

  return (
    <IonModal cssClass='time-options'
              isOpen={true}
              onDidDismiss={props.close}
              presentingElement={presentingElement}
              swipeToClose={true}>
      <IonHeader>
        <IonToolbar>
          <IonSegment value={anchor} onIonChange={(e) => setAnchor(e.detail.value)}>
            <IonSegmentButton value='departAfter'>
              Depart at
            </IonSegmentButton>
            <IonSegmentButton value='arriveBefore'>
              Arrive by
            </IonSegmentButton>
          </IonSegment>
        </IonToolbar>
      </IonHeader>
      <input min={today} onChange={onDateChange} required type="date" value={date ?? today}/>
      <input min={!date || date === today ? now : '00:00'}
             onChange={onTimeChange}
             required
             step='60'
             type="time"
             value={time ?? now}/>
      <IonButton disabled={!date && !time} fill='clear' onClick={onReset}>Reset to now</IonButton>
      <IonFooter>
        <IonToolbar>
          <IonButton slot='start' fill='clear' onClick={props.close}>Cancel</IonButton>
          <IonButton slot='end' onClick={() => {
            props.setOptions({anchor, date, time});
            props.close();
          }}>Set</IonButton>
        </IonToolbar>
      </IonFooter>
    </IonModal>
  );
};
