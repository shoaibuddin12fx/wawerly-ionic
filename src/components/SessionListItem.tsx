import React, { useRef } from 'react';
import { IonItemSliding, IonItem, IonLabel, IonItemOptions, IonItemOption, AlertButton } from '@ionic/react';
import { Session } from '../models/Schedule';

interface SessionListItemProps {
  session: Session;
  listType: "nearest" | "favourites";
  onAddFavourite: (id: string) => void;
  onRemoveFavourite: (id: string) => void;
  onShowAlert: (header: string, buttons: AlertButton[]) => void;
  isFavourite: boolean;
}

const SessionListItem: React.FC<SessionListItemProps> = ({ isFavourite, onAddFavourite, onRemoveFavourite, onShowAlert, session, listType }) => {
  const ionItemSlidingRef = useRef<HTMLIonItemSlidingElement>(null)

  const dismissAlert = () => {
    ionItemSlidingRef.current && ionItemSlidingRef.current.close();
  }

  const removeFavouriteSession = () => {
    onAddFavourite(session.id);
    onShowAlert('Favourite already added', [
      {
        text: 'Cancel',
        handler: dismissAlert
      },
      {
        text: 'Remove',
        handler: () => {
          onRemoveFavourite(session.id);
          dismissAlert();
        }
      }
    ]);
  }

  const addFavouriteSession = () => {
    if (isFavourite) {
      // woops, they already favourited it! What shall we do!?
      // prompt them to remove it
      removeFavouriteSession();
    } else {
      // remember this session as a user favourite
      onAddFavourite(session.id);
      onShowAlert('Favourite Added', [
        {
          text: 'OK',
          handler: dismissAlert
        }
      ]);
    }
  };

  return (
    <IonItemSliding ref={ionItemSlidingRef} class={'track-' + session.tracks[0].toLowerCase()}>
      <IonItem routerLink={`/tabs/schedule/${session.id}`}>
        <IonLabel>
          <h3>{session.name}</h3>
          <p>
            {session.timeStart}&mdash;&nbsp;
            {session.timeStart}&mdash;&nbsp;
            {session.location}
          </p>
        </IonLabel>
      </IonItem>
      <IonItemOptions>
        {listType === "favourites" ?
          <IonItemOption color="danger" onClick={() => removeFavouriteSession()}>
            Remove
          </IonItemOption>
          :
          <IonItemOption color="favourite" onClick={addFavouriteSession}>
            Favourite
          </IonItemOption>
        }
      </IonItemOptions>
    </IonItemSliding>
  );
};

export default React.memo(SessionListItem);
