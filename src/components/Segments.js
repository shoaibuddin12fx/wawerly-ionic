import React from 'react';
import {IonIcon} from '@ionic/react';
import {arrowForward} from 'ionicons/icons';

import {getIcon, ionicons} from './Icon';

import './Segments.scss';

export const unhash = (trip, segmentTemplates) => {
  trip.segments.forEach(
    segment =>
      segment.template = segmentTemplates.filter(template => template.hashCode === segment.segmentTemplateHashCode)[0],
  );
}

export const Segments = function ({trip}) {
  const segments = trip.segments.map((segment, s) => {
    const template = segment.template;
    // TODO: should parking (a car) or locking (a bike) or unlocking (a rental) get one of these subscript durations?
    if (template.modeIdentifier === 'wa_wal' ||
        template.modeInfo.identifier === 'stationary_transfer' ||
        template.modeInfo.identifier === 'stationary_wait') {
      const duration = Math.round((segment.endTime - segment.startTime) / 60);
      // Google maps only shows the walk if the first segment is longer
      // than 3 minutes, or another segment is longer than 2 minutes
      if (s === 0 ? duration > 3 : duration > 2) {
        if (duration < 100) {
          return <React.Fragment key={s}><IonIcon
            icon={ionicons[template.modeInfo.localIcon]}/><sub>{duration}</sub></React.Fragment>;
        } else {
          return <IonIcon icon={ionicons[template.modeInfo.localIcon]} key={s}/>;
        }
      } else {
        return null;
      }
    }
    return <React.Fragment key={s}>
      {getIcon(template.modeInfo)}{segment.serviceNumber}
    </React.Fragment>;
  }).filter(notnull => notnull);
  for (let s = segments.length - 1; s > 0; s--) {
    segments.splice(s, 0, <IonIcon key={`s${s}`} icon={arrowForward}/>);
  }
  return <div className='segments'>{segments}</div>;
};
