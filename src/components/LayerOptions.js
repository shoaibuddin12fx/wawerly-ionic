import React from 'react';
import {
  IonButton,
  IonCheckbox,
  IonContent,
  IonFooter,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonModal,
  IonTitle,
  IonToolbar,
} from '@ionic/react';

export const LayerOptions = ({presentingElement, ...props}) => {
    const [visibleOptions, setVisibleOptions]   = React.useState(props.visibleLayerOptions);

    const onIonChange = e => {
        if (e.detail.checked) {
          visibleOptions[e.detail.value] = true;
          setVisibleOptions(visibleOptions);
        } else {
          visibleOptions[e.detail.value] = false;
          setVisibleOptions(visibleOptions);
        }
      };

      return (
        <IonModal cssClass='layer-options'
        isOpen={true}
        onDidDismiss={props.close}
        presentingElement={presentingElement}
        swipeToClose={true}>
          <IonHeader>
            <IonToolbar>
              <IonTitle>Layers Options</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent className='outer-content'>
            <IonList>
              <IonListHeader><IonLabel>Details</IonLabel></IonListHeader>
              {props.resources.map(resource => 
                <IonItem key={resource.name}>
                  <IonLabel style={{ background: `hsl(${resource.hue}, 100%, 80%)` }}>
                    {resource.dataFeaturesLength} {resource.name}
                  </IonLabel>
                  <IonCheckbox checked={visibleOptions[resource.name]}
                    onIonChange={onIonChange}
                    value={resource.name}
                    slot='start' />
                </IonItem>
              )}
            </IonList>
          </IonContent>
          <IonFooter>
            <IonToolbar>
              <IonButton slot='start' fill='clear' onClick={props.close}>Cancel</IonButton>
              <IonButton slot='end' onClick={() => {
                props.setOptions(visibleOptions);
                props.close();
              }}>Done</IonButton>
            </IonToolbar>
          </IonFooter>
        </IonModal>
      )
}