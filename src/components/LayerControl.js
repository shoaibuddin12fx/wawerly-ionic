import React from 'react';
import {IonIcon} from '@ionic/react';
import {logoBuffer, toggle} from 'ionicons/icons';
import './LayerControl.scss';
import { LayerOptions } from './LayerOptions';
import {get, init, set} from '../util/storage';

// noinspection JSUnusedGlobalSymbols
const layerTypesGetOptions = {
  line:   hue => ({
    'type':   'line',
    'layout': {
      'line-join':  'round',
      'line-cap':   'round',
      // make layer visible by default
      'visibility': 'visible',
    },
    'paint':  {
      'line-width': {
        "stops": [
          // zoom is 13 -> line width will be 2px
          [13, 2],
          // zoom is 16 -> line width will be 4px
          [16, 4],
        ],
      },
      'line-color': `hsl(${hue}, 100%, 50%)`,
    },
  }),
  fill:   hue => ({
    'type':   'fill',
    'layout': {
      // make layer visible by default
      'visibility': 'visible',
    },
    'paint':  {
      'fill-color': `hsl(${hue}, 100%, 50%)`,
    },
  }),
  circle: hue => ({
    'type':   'circle',
    'layout': {
      // make layer visible by default
      'visibility': 'visible',
    },
    'paint':  {
      'circle-radius': {
        "stops": [
          // zoom is 13 -> circle radius will be 2px
          [13, 2],
          // zoom is 16 -> circle radius will be 16px
          [16, 16],
        ],
      },
      'circle-color':  `hsl(${hue}, 100%, 50%)`,
    },
  }),
};

init('layer-options', 
  ["Designated PUDO (Pick Up Drop Off zones) for ride share"]
);

export default function LayerControl({map}) {
  const [isOpen, setIsOpen]       = React.useState(false);
  const [resources, setResources] = React.useState([]);
  const [layerOptions, setLayerOptions]         = React.useState(get('layer-options'));
  const cacheTimeOptions                              =
  layerOptions => setLayerOptions(set('layer-options', layerOptions));
  
  const pageRef = React.useRef();
  

  const toggleLayer = resource => checked => {
    /** @type string */
    const id = resource.name;
    if (map.getLayer(id)) {
      map.setLayoutProperty(id, 'visibility', checked ? 'visible' : 'none');
    } else {
      if (checked) {
        const layerOptions = layerTypesGetOptions[resource.layerType](resource.hue);
        map.addLayer({
          'id': id,
          'source': {
            type: 'geojson',
            data: `/assets/data/geojson/${resource.geojson}`,
          },
          ...layerOptions,
        });
      }
    }
  };

  React.useEffect(() => {
    (async () => {
      const resources = await (await fetch('/assets/data/resources.json')).json();
      setResources(resources);
      // const pudoResources = resources.filter(resource => resource.name.includes('PUDO'));
      // const pudoResource  = pudoResources[0];
      // map.once('idle', () => toggleLayer(pudoResource)(true));
      resources.map(resource => {
        const layerName = layerOptions[resource.name];
        toggleLayer(resource)(layerName !== undefined && layerName !== false);
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layerOptions]);

  const resourceListToObject = (resources) => {
      return resources.reduce((result, resource) => {
        result[resource.name] = layerOptions[resource.name] !== false;
        return result;
      }, {});
  }

  return (
    <div className='layer-control'>
      <IonIcon icon={logoBuffer} onClick={() => setIsOpen(true)} style={{fontSize: '24px'}}/>
      {isOpen &&
       <>
      <LayerOptions close = {()=> setIsOpen(false)} 
                presentingElement = {pageRef.current}
                visibleLayerOptions = {resourceListToObject(resources)}
                setOptions = {cacheTimeOptions}
                resources = {resources} />
       </>}
    </div>
  );
}
