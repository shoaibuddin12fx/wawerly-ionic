import {bicycle, boat, bus, car, gitCommit, globe, key, school, time, train, walk} from 'ionicons/icons';
import React from 'react';
import {IonIcon} from '@ionic/react';

export const ionicons = {
  bus,
  car,
  collect:  key,
  bicycle,
  ferry:    boat,
  globe,
  subway:   train,
  school,
  train,
  transfer: gitCommit,
  wait:     time,
  walk,
};

// cache so we only fetch the icon once
const localIcons = {
  'shuttle':         <LocalIcon mode='shuttle'/>,
  'carNextDoor':     <LocalIcon mode='carNextDoor'/>,
  'motorbike':       <LocalIcon mode='motorbike'/>,
  'taxi':            <LocalIcon mode='taxi'/>,
  'publicTransport': <LocalIcon mode='publicTransport'/>,
  'school-bus':      <LocalIcon mode='school-bus'/>,
  'parking':         <LocalIcon mode='parking'/>,
  'tram':            <LocalIcon mode='tram'/>,
  'wheelchair':      <LocalIcon mode='wheelchair'/>,
};

function Icon({url}) {
  const [__html, set__HTML] = React.useState('');
  React.useEffect(() => {
    (async () => {
      const response = await fetch(url);
      const text     = await response.text();
      const doc = new DOMParser().parseFromString(text, 'image/svg+xml');
      doc.documentElement.removeAttribute('width');
      doc.documentElement.removeAttribute('height');
      const __html = new XMLSerializer().serializeToString(doc.documentElement);
      set__HTML(__html);
    })();
  }, [url]);
  return <span className="mode-icon" dangerouslySetInnerHTML={{__html}}/>;
}

function LocalIcon({mode}) {
  return <Icon url={`/assets/icon/modeicons/ic-${mode}-24px.svg`}/>;
}

function RemoteIcon({mode}) {
  return <Icon url={`https://api.tripgo.com/v1/modeicons/icon-mode-${mode}.svg`}/>
}

// TODO: ModeInfoIcon component
export function getIcon(modeInfo) {
  if (modeInfo.localIcon in ionicons) {
    return <IonIcon icon={ionicons[modeInfo.localIcon]}/>;
  }
  if (modeInfo.localIcon in localIcons) {
    return localIcons[modeInfo.localIcon];
  }
  if (modeInfo.remoteIcon) {
    return <RemoteIcon mode={modeInfo.remoteIcon}/>;
  }
  return modeInfo.identifier;
}

// TODO: ModeIcon component
export function getModeIcon(mode) {
  return mode in ionicons ?
         <IonIcon icon={ionicons[mode]}/> :
         mode in localIcons ?
         localIcons[mode] :
         <RemoteIcon mode={mode}/>;
}
