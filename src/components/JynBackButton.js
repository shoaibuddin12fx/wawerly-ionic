import React from 'react';
import {useHistory} from 'react-router';
import {IonBackButton} from '@ionic/react';

export default (props) => {
  const history = useHistory();
  const onClick = () => history.goBack();
  return <IonBackButton onClick={onClick} {...props}/>;
};
