import React, {useState} from 'react';
import {
  IonButton,
  IonCheckbox,
  IonContent,
  IonFooter,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonTitle,
  IonToolbar,
} from '@ionic/react';

const getCheckboxItem = (state, setState) => (label, value) => {
  const onIonChange = e => {
    if (e.detail.checked) {
      setState([...state, value]);
    } else {
      setState(state.filter(stateValue => stateValue !== value));
    }
  };

  return <IonItem>
    <IonLabel>{label}</IonLabel>
    <IonCheckbox checked={state.includes(value)}
                 onIonChange={onIonChange}
                 slot='start'/>
  </IonItem>
};

/**
 * @type React.FC
 */
export default ({onDismissModal, setFilter, ...props}) => {
  const [modes, setModes]   = useState(props.modes);
  const [radius, setRadius] = useState(props.radius);

  const getModeItem = getCheckboxItem(modes, setModes);

  return (
    <>
      <IonHeader>
        <IonToolbar>
          <IonTitle>
            Filter Stops/Services
          </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent class="outer-content">
        <IonList>
          <IonListHeader><IonLabel>Distance</IonLabel></IonListHeader>
          <IonItem>
            <IonLabel>Metres</IonLabel>
            <input min={500}
                   max={2000}
                   onBlur={e => setRadius(e.target.valueAsNumber < 500 ? 500 : e.target.valueAsNumber < 2000 ? e.target.valueAsNumber : 2000)}
                   onChange={e => setRadius(e.target.valueAsNumber)}
                   required
                   slot='end'
                   type="number"
                   value={radius}/>
          </IonItem>
        </IonList>
        <IonList>
          <IonListHeader><IonLabel>Modes</IonLabel></IonListHeader>
          {getModeItem('Bus', 'bus')}
          {getModeItem('Ferry', 'ferry')}
          {getModeItem('Metro', 'metro')}
          {getModeItem('Train', 'train')}
          {getModeItem('Tram and light rail', 'tram')}
        </IonList>
      </IonContent>
      <IonFooter>
        <IonToolbar>
          <IonButton slot='start' fill='clear' onClick={onDismissModal}>Cancel</IonButton>
          <IonButton slot='end' onClick={() => {
            setFilter({modes, radius});
            onDismissModal();
          }}>Set</IonButton>
        </IonToolbar>
      </IonFooter>
    </>
  );
};
