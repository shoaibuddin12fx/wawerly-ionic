import {IonLabel, IonNote} from '@ionic/react';
import React from 'react';

import {getStringFromSeconds, hourMinuteFormat} from '../util/time';

import './ResultGroup.scss';

export function ResultGroup({children, trip}) {
  return <>
    <IonLabel>
      <div>
        {children}
      </div>
      <div>
        {hourMinuteFormat.format(new Date(1000 * trip.depart))}
        {' - '}
        {hourMinuteFormat.format(new Date(1000 * trip.arrive))}
      </div>
    </IonLabel>
    <IonNote className='result-group-duration-price' slot='end'>
      <div>
        {getStringFromSeconds(trip.arrive - trip.depart)}
      </div>
      <div>
        {trip.moneyCost === 0 ? 'FREE' :
         trip.moneyCost ? `${trip.currencySymbol}${trip.moneyCost.toFixed(2)}` :
         undefined}
      </div>
    </IonNote>
  </>
}
