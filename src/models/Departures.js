export default {
   "parentInfo" : {
      "lng" : 151.08224,
      "timezone" : "Australia/Sydney",
      "region" : "AU_NSW_Sydney",
      "id" : "pt_pub|AU_NSW_Sydney|212210",
      "address" : "Eastwood Station",
      "stopType" : "train",
      "modeInfo" : {
         "localIcon" : "train",
         "identifier" : "pt_pub_train",
         "alt" : "train"
      },
      "children" : [
         {
            "lat" : -33.79014,
            "code" : "2122234",
            "name" : "Eastwood Station Platform 4",
            "services" : "CCN, NRC, NRW, T9",
            "class" : "StopLocation",
            "shortName" : "Platform 4",
            "popularity" : 2650,
            "stopCode" : "2122234",
            "timezone" : "Australia/Sydney",
            "lng" : 151.08209,
            "region" : "AU_NSW_Sydney",
            "id" : "pt_pub|AU_NSW_Sydney|2122234",
            "wheelchairAccessible" : true,
            "address" : "Eastwood Station Platform 4",
            "modeInfo" : {
               "localIcon" : "train",
               "identifier" : "pt_pub_train",
               "alt" : "train"
            },
            "stopType" : "train"
         },
         {
            "shortName" : "Platform 1",
            "services" : "T1, T9, CCN, NRC, NRW",
            "class" : "StopLocation",
            "stopCode" : "2122231",
            "popularity" : 5550,
            "lat" : -33.79015,
            "name" : "Eastwood Station Platform 1",
            "code" : "2122231",
            "lng" : 151.0824,
            "timezone" : "Australia/Sydney",
            "stopType" : "train",
            "modeInfo" : {
               "localIcon" : "train",
               "alt" : "train",
               "identifier" : "pt_pub_train"
            },
            "region" : "AU_NSW_Sydney",
            "address" : "Eastwood Station Platform 1",
            "wheelchairAccessible" : true,
            "id" : "pt_pub|AU_NSW_Sydney|2122231"
         },
         {
            "wheelchairAccessible" : true,
            "id" : "pt_pub|AU_NSW_Sydney|2122233",
            "address" : "Eastwood Station Platform 3",
            "region" : "AU_NSW_Sydney",
            "modeInfo" : {
               "localIcon" : "train",
               "alt" : "train",
               "identifier" : "pt_pub_train"
            },
            "stopType" : "train",
            "lng" : 151.08219,
            "timezone" : "Australia/Sydney",
            "code" : "2122233",
            "name" : "Eastwood Station Platform 3",
            "lat" : -33.79014,
            "popularity" : 6855,
            "stopCode" : "2122233",
            "class" : "StopLocation",
            "services" : "T9, CCN, NRW",
            "shortName" : "Platform 3"
         },
         {
            "stopType" : "train",
            "modeInfo" : {
               "localIcon" : "train",
               "alt" : "train",
               "identifier" : "pt_pub_train"
            },
            "region" : "AU_NSW_Sydney",
            "id" : "pt_pub|AU_NSW_Sydney|2122232",
            "wheelchairAccessible" : true,
            "address" : "Eastwood Station Platform 2",
            "lng" : 151.08232,
            "timezone" : "Australia/Sydney",
            "name" : "Eastwood Station Platform 2",
            "code" : "2122232",
            "lat" : -33.79015,
            "stopCode" : "2122232",
            "popularity" : 3945,
            "shortName" : "Platform 2",
            "services" : "T9, CCN, NRW",
            "class" : "StopLocation"
         }
      ],
      "services" : "",
      "class" : "StopLocation",
      "popularity" : 19000,
      "stopCode" : "212210",
      "lat" : -33.79015,
      "code" : "212210",
      "name" : "Eastwood Station"
   },
   "embarkationStops" : [
      {
         "services" : [
            {
               "operator" : "Sydney Metro",
               "startTime" : 1594004160,
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "green" : 131,
                  "blue" : 136,
                  "red" : 22
               },
               "operatorID" : "SMNW",
               "mode" : "metro",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceDirection" : "Tallawong",
               "realtimeVehicle" : {
                  "id" : "111-11",
                  "location" : {
                     "lng" : 151.18089,
                     "bearing" : 359,
                     "lat" : -33.797
                  },
                  "lastUpdate" : 1594003984,
                  "label" : "12:43pm Chatswood - Tallawong"
               },
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "realTimeDeparture" : 1594004557,
               "serviceTripID" : "111-11.060720.15.1243",
               "serviceNumber" : "M",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               }
            },
            {
               "operatorID" : "SMNW",
               "mode" : "metro",
               "operator" : "Sydney Metro",
               "startTime" : 1594004760,
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "green" : 131,
                  "blue" : 136,
                  "red" : 22
               },
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceDirection" : "Tallawong",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "realTimeDeparture" : 1594005141,
               "serviceTripID" : "112-11.060720.15.1253",
               "serviceNumber" : "M"
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "realTimeDeparture" : 1594005764,
               "serviceNumber" : "M",
               "serviceTripID" : "114-11.060720.15.1303",
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "serviceDirection" : "Tallawong",
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "operatorID" : "SMNW",
               "mode" : "metro",
               "operator" : "Sydney Metro",
               "startTime" : 1594005360,
               "serviceColor" : {
                  "red" : 22,
                  "green" : 131,
                  "blue" : 136
               },
               "serviceName" : "Metro North West Line"
            },
            {
               "operatorID" : "SMNW",
               "mode" : "metro",
               "startTime" : 1594005960,
               "operator" : "Sydney Metro",
               "serviceColor" : {
                  "red" : 22,
                  "blue" : 136,
                  "green" : 131
               },
               "serviceName" : "Metro North West Line",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceDirection" : "Tallawong",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               },
               "realTimeDeparture" : 1594006409,
               "serviceTripID" : "115-11.060720.15.1313",
               "serviceNumber" : "M"
            },
            {
               "serviceDirection" : "Tallawong",
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realTimeDeparture" : 1594007010,
               "serviceNumber" : "M",
               "serviceTripID" : "117-11.060720.15.1323",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro"
               },
               "startTime" : 1594006560,
               "operator" : "Sydney Metro",
               "serviceColor" : {
                  "red" : 22,
                  "blue" : 136,
                  "green" : 131
               },
               "serviceName" : "Metro North West Line",
               "operatorID" : "SMNW",
               "mode" : "metro",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true
            },
            {
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "serviceDirection" : "Tallawong",
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "M",
               "serviceTripID" : "118-11.060720.15.1333",
               "realTimeDeparture" : 1594007609,
               "mode" : "metro",
               "operatorID" : "SMNW",
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "red" : 22,
                  "green" : 131,
                  "blue" : 136
               },
               "operator" : "Sydney Metro",
               "startTime" : 1594007160,
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME"
            },
            {
               "serviceDirection" : "Tallawong",
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realTimeDeparture" : 1594007790,
               "serviceNumber" : "M",
               "serviceTripID" : "120-11.060720.15.1343",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro",
                  "localIcon" : "subway"
               },
               "startTime" : 1594007760,
               "operator" : "Sydney Metro",
               "serviceColor" : {
                  "red" : 22,
                  "blue" : 136,
                  "green" : 131
               },
               "serviceName" : "Metro North West Line",
               "operatorID" : "SMNW",
               "mode" : "metro",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true
            },
            {
               "mode" : "metro",
               "operatorID" : "SMNW",
               "serviceColor" : {
                  "red" : 22,
                  "blue" : 136,
                  "green" : 131
               },
               "serviceName" : "Metro North West Line",
               "operator" : "Sydney Metro",
               "startTime" : 1594008360,
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceDirection" : "Tallawong",
               "modeInfo" : {
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro",
                  "localIcon" : "subway"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "109-13.060720.15.1353",
               "serviceNumber" : "M",
               "realTimeDeparture" : 1594008858
            },
            {
               "operatorID" : "SMNW",
               "mode" : "metro",
               "operator" : "Sydney Metro",
               "startTime" : 1594008960,
               "serviceColor" : {
                  "green" : 131,
                  "blue" : 136,
                  "red" : 22
               },
               "serviceName" : "Metro North West Line",
               "bicycleAccessible" : true,
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "searchString" : "Cherrybrook Station; Castle Hill Station; Hills Showground Station; Norwest Station; Bella Vista Station; Kellyville Station; Rouse Hill Station; Tallawong Station",
               "serviceDirection" : "Tallawong",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               },
               "serviceTripID" : "111-13.060720.15.1403",
               "serviceNumber" : "M"
            }
         ],
         "stopCode" : "2121226",
         "wheelchairAccessible" : true
      },
      {
         "stopCode" : "2121222",
         "wheelchairAccessible" : true,
         "services" : [
            {
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "T9",
               "serviceTripID" : "143K.1382.133.12.A.8.62453184",
               "realTimeDeparture" : 1594004700,
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "label" : "12:31 Central Station to Hornsby Station ",
                  "lastUpdate" : 1594003934,
                  "components" : [
                     [
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        }
                     ]
                  ],
                  "location" : {
                     "lng" : 151.08717,
                     "lat" : -33.82986,
                     "bearing" : 12
                  },
                  "id" : "143K.1382"
               },
               "serviceDirection" : "Hornsby",
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "realtimeVehicleAlternatives" : [
                  {
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "label" : "",
                     "lastUpdate" : 1594003919
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lat" : -34.10328,
                        "bearing" : 221,
                        "lng" : 150.99471
                     }
                  },
                  {
                     "lastUpdate" : 1594003900,
                     "label" : "",
                     "location" : {
                        "bearing" : 26,
                        "lat" : -33.75104,
                        "lng" : 150.61253
                     },
                     "id" : "NonTimetabled.1877"
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lng" : 151.07608,
                        "bearing" : 283,
                        "lat" : -33.86466
                     },
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "id" : "NonTimetabled.2126",
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "location" : {
                        "lng" : 151.0753,
                        "lat" : -33.86473,
                        "bearing" : 95
                     },
                     "id" : "NonTimetabled.CA04",
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251",
                     "lastUpdate" : 1594003964,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003603,
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               },
               "operator" : "Sydney Trains",
               "startTime" : 1594004700
            },
            {
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceDirection" : "Hornsby",
               "realtimeVehicle" : {
                  "location" : {
                     "bearing" : 290,
                     "lat" : -33.89413,
                     "lng" : 151.1675
                  },
                  "id" : "115M.1382",
                  "label" : "12:46 Central Station to Hornsby Station ",
                  "lastUpdate" : 1594003949
               },
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "serviceTripID" : "115M.1382.133.12.T.8.62450990",
               "serviceNumber" : "T9",
               "realTimeDeparture" : 1594005630,
               "modeInfo" : {
                  "localIcon" : "train",
                  "identifier" : "pt_pub_train",
                  "alt" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "green" : 31,
                  "blue" : 47,
                  "red" : 209
               },
               "operator" : "Sydney Trains",
               "startTime" : 1594005630,
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "realtimeVehicleAlternatives" : [
                  {
                     "label" : "",
                     "lastUpdate" : 1594003919,
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lat" : -34.10328,
                        "bearing" : 221,
                        "lng" : 150.99471
                     }
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003900,
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     },
                     "id" : "NonTimetabled.1877"
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "lastUpdate" : 1594003908,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lng" : 151.05457,
                        "bearing" : 307,
                        "lat" : -33.86336
                     }
                  },
                  {
                     "location" : {
                        "lat" : -33.86473,
                        "bearing" : 95,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04",
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     },
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lat" : -33.80713,
                        "bearing" : 203,
                        "lng" : 151.09024
                     },
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ]
            },
            {
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "location" : {
                        "bearing" : 90,
                        "lat" : -33.747,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96",
                     "label" : "",
                     "lastUpdate" : 1594003919
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     }
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     },
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "location" : {
                        "bearing" : 283,
                        "lat" : -33.86466,
                        "lng" : 151.07608
                     },
                     "id" : "NonTimetabled.1BM9"
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "location" : {
                        "lng" : 151.05457,
                        "bearing" : 307,
                        "lat" : -33.86336
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "location" : {
                        "lat" : -33.86473,
                        "bearing" : 95,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04",
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003964,
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     }
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     },
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "operator" : "Sydney Trains",
               "startTime" : 1594006500,
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               },
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "realTimeDeparture" : 1594006500,
               "serviceTripID" : "148N.1382.133.12.A.8.62452845",
               "serviceNumber" : "T9",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceDirection" : "Hornsby",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               }
            },
            {
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceDirection" : "Hornsby",
               "modeInfo" : {
                  "localIcon" : "train",
                  "identifier" : "pt_pub_train",
                  "alt" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "111J.1382.133.12.T.8.62457215",
               "serviceNumber" : "T9",
               "realTimeDeparture" : 1594007400,
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               },
               "serviceName" : "T9 Northern Line",
               "operator" : "Sydney Trains",
               "startTime" : 1594007400,
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lat" : -34.10328,
                        "bearing" : 221,
                        "lng" : 150.99471
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     },
                     "id" : "NonTimetabled.1877",
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "location" : {
                        "lng" : 151.07608,
                        "bearing" : 283,
                        "lat" : -33.86466
                     },
                     "id" : "NonTimetabled.1BM9"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003399,
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lat" : -33.88278,
                        "bearing" : 278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "location" : {
                        "lat" : -33.80713,
                        "bearing" : 203,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07",
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME"
            },
            {
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceDirection" : "Hornsby",
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "serviceTripID" : "112K.1382.133.4.T.8.62452429",
               "serviceNumber" : "T9",
               "realTimeDeparture" : 1594008300,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceColor" : {
                  "blue" : 47,
                  "green" : 31,
                  "red" : 209
               },
               "serviceName" : "T9 Northern Line",
               "startTime" : 1594008300,
               "operator" : "Sydney Trains",
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     },
                     "label" : "",
                     "lastUpdate" : 1594003610
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003900,
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     }
                  },
                  {
                     "location" : {
                        "bearing" : 283,
                        "lat" : -33.86466,
                        "lng" : 151.07608
                     },
                     "id" : "NonTimetabled.1BM9",
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "location" : {
                        "bearing" : 307,
                        "lat" : -33.86336,
                        "lng" : 151.05457
                     },
                     "id" : "NonTimetabled.2126",
                     "lastUpdate" : 1594003399,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "lng" : 151.0753,
                        "lat" : -33.86473,
                        "bearing" : 95
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003603,
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     }
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "label" : "",
                     "lastUpdate" : 1594003822
                  }
               ]
            }
         ]
      },
      {
         "services" : [
            {
               "realtimeVehicle" : {
                  "id" : "204E.1382",
                  "location" : {
                     "lat" : -33.89142,
                     "bearing" : 280,
                     "lng" : 151.14207
                  },
                  "label" : "12:45 Central Station to Newcastle Interchange Station ",
                  "lastUpdate" : 1594003948
               },
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station; Asquith Station; Mount Colah Station; Mount Kuringgai Station; Berowra Station; Cowan Station; Hawkesbury River Station; Wondabyne Station; Woy Woy Station; Koolewong Station; Tascott Station; Point Clare Station; Gosford Station; Narara Station; Niagara Park Station; Lisarow Station; Ourimbah Station; Tuggerah Station; Wyong Station; Warnervale Station; Wyee Station; Morisset Station; Dora Creek Station; Awaba Station; Fassifern Station; Booragul Station; Teralba Station; Cockle Creek Station; Cardiff Station; Kotara Station; Adamstown Station; Broadmeadow Station; Hamilton Station; Newcastle Interchange Station",
               "serviceDirection" : "Newcastle Interchange",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "realTimeDeparture" : 1594005030,
               "serviceTripID" : "204E.1382.133.4.H.4.62453290",
               "serviceNumber" : "CCN",
               "operatorID" : "NSWTrains",
               "mode" : "train",
               "startTime" : 1594005030,
               "operator" : "NSW Trains",
               "serviceName" : "Central Coast & Newcastle Line",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               },
               "realtimeVehicleAlternatives" : [
                  {
                     "location" : {
                        "bearing" : 90,
                        "lat" : -33.747,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96",
                     "lastUpdate" : 1594003919,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     },
                     "id" : "NonTimetabled.1877",
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "bearing" : 283,
                        "lat" : -33.86466,
                        "lng" : 151.07608
                     },
                     "lastUpdate" : 1594003908,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     },
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     }
                  },
                  {
                     "location" : {
                        "lat" : -33.88278,
                        "bearing" : 278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251",
                     "lastUpdate" : 1594003964,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07",
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "label" : "",
                     "lastUpdate" : 1594003822
                  }
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true
            },
            {
               "serviceDirection" : "Newcastle Interchange",
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station; Asquith Station; Mount Colah Station; Mount Kuringgai Station; Berowra Station; Cowan Station; Hawkesbury River Station; Wondabyne Station; Woy Woy Station; Koolewong Station; Tascott Station; Point Clare Station; Gosford Station; Narara Station; Niagara Park Station; Lisarow Station; Ourimbah Station; Tuggerah Station; Wyong Station; Warnervale Station; Wyee Station; Morisset Station; Dora Creek Station; Awaba Station; Fassifern Station; Booragul Station; Teralba Station; Cockle Creek Station; Cardiff Station; Kotara Station; Adamstown Station; Broadmeadow Station; Hamilton Station; Newcastle Interchange Station",
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "realTimeDeparture" : 1594028430,
               "serviceNumber" : "CCN",
               "serviceTripID" : "N151.1382.133.4.V.4.62449704",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "operator" : "NSW Trains",
               "startTime" : 1594006830,
               "serviceColor" : {
                  "green" : 31,
                  "blue" : 47,
                  "red" : 209
               },
               "serviceName" : "Central Coast & Newcastle Line",
               "operatorID" : "NSWTrains",
               "mode" : "train",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     }
                  },
                  {
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     },
                     "id" : "NonTimetabled.1877",
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "id" : "NonTimetabled.1BM9",
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     },
                     "id" : "NonTimetabled.CA04"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003964,
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     }
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "location" : {
                        "lng" : 151.09024,
                        "lat" : -33.80713,
                        "bearing" : 203
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003822,
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "id" : "NonTimetabled.4190"
                  }
               ]
            },
            {
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     },
                     "id" : "NonTimetabled.1877",
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "id" : "NonTimetabled.1BM9"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003399,
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     },
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lat" : -33.88278,
                        "bearing" : 278,
                        "lng" : 150.9939
                     },
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07",
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "bicycleAccessible" : true,
               "operatorID" : "NSWTrains",
               "mode" : "train",
               "startTime" : 1594008630,
               "operator" : "NSW Trains",
               "serviceName" : "Central Coast & Newcastle Line",
               "serviceColor" : {
                  "green" : 31,
                  "blue" : 47,
                  "red" : 209
               },
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "serviceTripID" : "N153.1382.133.4.V.4.62450173",
               "serviceNumber" : "CCN",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "searchString" : "Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station; Asquith Station; Mount Colah Station; Mount Kuringgai Station; Berowra Station; Cowan Station; Hawkesbury River Station; Wondabyne Station; Woy Woy Station; Koolewong Station; Tascott Station; Point Clare Station; Gosford Station; Narara Station; Niagara Park Station; Lisarow Station; Ourimbah Station; Tuggerah Station; Wyong Station; Warnervale Station; Wyee Station; Morisset Station; Dora Creek Station; Awaba Station; Fassifern Station; Booragul Station; Teralba Station; Cockle Creek Station; Cardiff Station; Kotara Station; Adamstown Station; Broadmeadow Station; Hamilton Station; Newcastle Interchange Station",
               "serviceDirection" : "Newcastle Interchange"
            }
         ],
         "wheelchairAccessible" : true,
         "stopCode" : "2121223"
      },
      {
         "services" : [
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "realTimeDeparture" : 1594004669,
               "serviceNumber" : "M",
               "serviceTripID" : "115-10.060720.15.1231",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "id" : "115-10",
                  "location" : {
                     "lat" : -33.72763,
                     "bearing" : 69,
                     "lng" : 150.98778
                  },
                  "label" : "12:31pm Tallawong - Chatswood",
                  "lastUpdate" : 1594003984
               },
               "serviceDirection" : "Chatswood",
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "operatorID" : "SMNW",
               "mode" : "metro",
               "operator" : "Sydney Metro",
               "startTime" : 1594004040,
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "red" : 22,
                  "green" : 131,
                  "blue" : 136
               }
            },
            {
               "startTime" : 1594004640,
               "operator" : "Sydney Metro",
               "serviceColor" : {
                  "green" : 131,
                  "blue" : 136,
                  "red" : 22
               },
               "serviceName" : "Metro North West Line",
               "operatorID" : "SMNW",
               "mode" : "metro",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "serviceDirection" : "Chatswood",
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "realtimeVehicle" : {
                  "label" : "12:41pm Tallawong - Chatswood",
                  "lastUpdate" : 1594003984,
                  "location" : {
                     "bearing" : 145,
                     "lat" : -33.69233,
                     "lng" : 150.92464
                  },
                  "id" : "117-10"
               },
               "realTimeDeparture" : 1594005270,
               "serviceNumber" : "M",
               "serviceTripID" : "117-10.060720.15.1241",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               }
            },
            {
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceDirection" : "Chatswood",
               "modeInfo" : {
                  "localIcon" : "subway",
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "118-10.060720.15.1251",
               "serviceNumber" : "M",
               "realTimeDeparture" : 1594005869,
               "mode" : "metro",
               "operatorID" : "SMNW",
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "red" : 22,
                  "blue" : 136,
                  "green" : 131
               },
               "startTime" : 1594005240,
               "operator" : "Sydney Metro",
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME"
            },
            {
               "startTime" : 1594005840,
               "operator" : "Sydney Metro",
               "serviceColor" : {
                  "blue" : 136,
                  "green" : 131,
                  "red" : 22
               },
               "serviceName" : "Metro North West Line",
               "operatorID" : "SMNW",
               "mode" : "metro",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceDirection" : "Chatswood",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "realTimeDeparture" : 1594005927,
               "serviceTripID" : "120-10.060720.15.1301",
               "serviceNumber" : "M",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro",
                  "localIcon" : "subway"
               }
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               },
               "realTimeDeparture" : 1594007118,
               "serviceTripID" : "109-12.060720.15.1311",
               "serviceNumber" : "M",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceDirection" : "Chatswood",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "operatorID" : "SMNW",
               "mode" : "metro",
               "operator" : "Sydney Metro",
               "startTime" : 1594006440,
               "serviceColor" : {
                  "red" : 22,
                  "blue" : 136,
                  "green" : 131
               },
               "serviceName" : "Metro North West Line"
            },
            {
               "operatorID" : "SMNW",
               "mode" : "metro",
               "startTime" : 1594007040,
               "operator" : "Sydney Metro",
               "serviceColor" : {
                  "blue" : 136,
                  "green" : 131,
                  "red" : 22
               },
               "serviceName" : "Metro North West Line",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "serviceDirection" : "Chatswood",
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro",
                  "localIcon" : "subway"
               },
               "realTimeDeparture" : 1594007497,
               "serviceNumber" : "M",
               "serviceTripID" : "111-12.060720.15.1321"
            },
            {
               "mode" : "metro",
               "operatorID" : "SMNW",
               "serviceColor" : {
                  "red" : 22,
                  "green" : 131,
                  "blue" : 136
               },
               "serviceName" : "Metro North West Line",
               "startTime" : 1594007640,
               "operator" : "Sydney Metro",
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceDirection" : "Chatswood",
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "112-12.060720.15.1331",
               "serviceNumber" : "M",
               "realTimeDeparture" : 1594008081
            },
            {
               "serviceTripID" : "114-12.060720.15.1341",
               "serviceNumber" : "M",
               "realTimeDeparture" : 1594008704,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               },
               "wheelchairAccessible" : true,
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceDirection" : "Chatswood",
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "red" : 22,
                  "green" : 131,
                  "blue" : 136
               },
               "operator" : "Sydney Metro",
               "startTime" : 1594008240,
               "mode" : "metro",
               "operatorID" : "SMNW"
            },
            {
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "mode" : "metro",
               "operatorID" : "SMNW",
               "serviceName" : "Metro North West Line",
               "serviceColor" : {
                  "green" : 131,
                  "blue" : 136,
                  "red" : 22
               },
               "operator" : "Sydney Metro",
               "startTime" : 1594008840,
               "modeInfo" : {
                  "localIcon" : "subway",
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "115-12.060720.15.1351",
               "serviceNumber" : "M",
               "realTimeDeparture" : 1594009349,
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "searchString" : "Macquarie University Station; Macquarie Park Station; North Ryde Station; Chatswood Station",
               "serviceDirection" : "Chatswood"
            }
         ],
         "stopCode" : "2121225",
         "wheelchairAccessible" : true
      },
      {
         "services" : [
            {
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "label" : "",
                     "lastUpdate" : 1594003919,
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     }
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "bearing" : 26,
                        "lat" : -33.75104,
                        "lng" : 150.61253
                     },
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "lastUpdate" : 1594003908,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003399,
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     },
                     "id" : "NonTimetabled.CA04",
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     }
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lat" : -33.80713,
                        "bearing" : 203,
                        "lng" : 151.09024
                     },
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lat" : -33.55813,
                        "bearing" : 236,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "operator" : "Sydney Trains",
               "startTime" : 1594004490,
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               },
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "realTimeDeparture" : 1594004490,
               "serviceNumber" : "T9",
               "serviceTripID" : "143K.1382.133.12.A.8.62453184",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "serviceDirection" : "Hornsby",
               "searchString" : "Epping Station; Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "components" : [
                     [
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        }
                     ]
                  ],
                  "id" : "143K.1382",
                  "location" : {
                     "lat" : -33.82986,
                     "bearing" : 12,
                     "lng" : 151.08717
                  },
                  "label" : "12:31 Central Station to Hornsby Station ",
                  "lastUpdate" : 1594003934
               }
            },
            {
               "serviceDirection" : "Hornsby",
               "searchString" : "Epping Station; Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "location" : {
                     "bearing" : 290,
                     "lat" : -33.89413,
                     "lng" : 151.1675
                  },
                  "id" : "115M.1382",
                  "label" : "12:46 Central Station to Hornsby Station ",
                  "lastUpdate" : 1594003949
               },
               "serviceNumber" : "T9",
               "serviceTripID" : "115M.1382.133.12.T.8.62450990",
               "realTimeDeparture" : 1594005426,
               "modeInfo" : {
                  "localIcon" : "train",
                  "identifier" : "pt_pub_train",
                  "alt" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceColor" : {
                  "green" : 31,
                  "blue" : 47,
                  "red" : 209
               },
               "serviceName" : "T9 Northern Line",
               "operator" : "Sydney Trains",
               "startTime" : 1594005426,
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "realtimeVehicleAlternatives" : [
                  {
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "id" : "NonTimetabled.CA96",
                     "lastUpdate" : 1594003919,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     },
                     "id" : "NonTimetabled.1921",
                     "label" : "",
                     "lastUpdate" : 1594003610
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003908,
                     "label" : "",
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     }
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "bearing" : 307,
                        "lat" : -33.86336,
                        "lng" : 151.05457
                     }
                  },
                  {
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "lat" : -33.86473,
                        "bearing" : 95,
                        "lng" : 151.0753
                     },
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003822,
                     "label" : "",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190"
                  }
               ]
            },
            {
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "serviceDirection" : "Hornsby",
               "searchString" : "Epping Station; Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "realTimeDeparture" : 1594006290,
               "serviceNumber" : "T9",
               "serviceTripID" : "148N.1382.133.12.A.8.62452845",
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "operator" : "Sydney Trains",
               "startTime" : 1594006290,
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line",
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "id" : "NonTimetabled.1921",
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "id" : "NonTimetabled.1877",
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "location" : {
                        "lng" : 151.07608,
                        "lat" : -33.86466,
                        "bearing" : 283
                     },
                     "id" : "NonTimetabled.1BM9"
                  },
                  {
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "id" : "NonTimetabled.2126",
                     "lastUpdate" : 1594003399,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003217,
                     "label" : "",
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     }
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     }
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003603,
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true
            },
            {
               "searchString" : "Epping Station; Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceDirection" : "Hornsby",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "serviceTripID" : "111J.1382.133.12.T.8.62457215",
               "serviceNumber" : "T9",
               "realTimeDeparture" : 1594007196,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "wheelchairAccessible" : true,
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "startTime" : 1594007196,
               "operator" : "Sydney Trains",
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "realtimeVehicleAlternatives" : [
                  {
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     },
                     "id" : "NonTimetabled.CA96",
                     "label" : "",
                     "lastUpdate" : 1594003919
                  },
                  {
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lng" : 151.07608,
                        "bearing" : 283,
                        "lat" : -33.86466
                     },
                     "id" : "NonTimetabled.1BM9",
                     "lastUpdate" : 1594003908,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "bearing" : 307,
                        "lat" : -33.86336,
                        "lng" : 151.05457
                     },
                     "id" : "NonTimetabled.2126",
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     },
                     "id" : "NonTimetabled.CA04",
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003964,
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     },
                     "id" : "NonTimetabled.T251"
                  },
                  {
                     "location" : {
                        "lng" : 151.09024,
                        "lat" : -33.80713,
                        "bearing" : 203
                     },
                     "id" : "NonTimetabled.CA07",
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ]
            },
            {
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "green" : 31,
                  "blue" : 47,
                  "red" : 209
               },
               "operator" : "Sydney Trains",
               "startTime" : 1594008090,
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     }
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     }
                  },
                  {
                     "location" : {
                        "bearing" : 26,
                        "lat" : -33.75104,
                        "lng" : 150.61253
                     },
                     "id" : "NonTimetabled.1877",
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lng" : 151.07608,
                        "bearing" : 283,
                        "lat" : -33.86466
                     },
                     "lastUpdate" : 1594003908,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "bearing" : 307,
                        "lat" : -33.86336,
                        "lng" : 151.05457
                     },
                     "lastUpdate" : 1594003399,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003217,
                     "label" : "",
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     },
                     "id" : "NonTimetabled.CA04"
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251"
                  },
                  {
                     "location" : {
                        "lat" : -33.80713,
                        "bearing" : 203,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07",
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "label" : "",
                     "lastUpdate" : 1594003822
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "serviceDirection" : "Hornsby",
               "searchString" : "Epping Station; Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "T9",
               "serviceTripID" : "112K.1382.133.4.T.8.62452429",
               "realTimeDeparture" : 1594008090
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "realTimeDeparture" : 1594009026,
               "serviceTripID" : "154H.1382.133.12.A.8.62449857",
               "serviceNumber" : "T9",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "searchString" : "Epping Station; Cheltenham Station; Beecroft Station; Pennant Hills Station; Thornleigh Station; Normanhurst Station; Hornsby Station",
               "serviceDirection" : "Hornsby",
               "realtimeVehicleAlternatives" : [
                  {
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     },
                     "lastUpdate" : 1594003919,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     },
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "bearing" : 26,
                        "lat" : -33.75104,
                        "lng" : 150.61253
                     },
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "location" : {
                        "lng" : 151.07608,
                        "lat" : -33.86466,
                        "bearing" : 283
                     },
                     "id" : "NonTimetabled.1BM9"
                  },
                  {
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     },
                     "lastUpdate" : 1594003399,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "lat" : -33.86473,
                        "bearing" : 95,
                        "lng" : 151.0753
                     },
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "id" : "NonTimetabled.4190",
                     "label" : "",
                     "lastUpdate" : 1594003822
                  }
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "startTime" : 1594009026,
               "operator" : "Sydney Trains",
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               }
            }
         ],
         "wheelchairAccessible" : true,
         "stopCode" : "2122233"
      },
      {
         "services" : [
            {
               "serviceTripID" : "969130",
               "serviceNumber" : "545",
               "modeInfo" : {
                  "alt" : "bus",
                  "identifier" : "pt_pub_bus",
                  "localIcon" : "bus"
               },
               "wheelchairAccessible" : true,
               "searchString" : "Eastwood Station West Pde; Eastwood Station Railway Pde; May St before Blaxland Rd; Moore Park, Balaclava Rd; Balaclava Rd opp Gordon St; Balaclava Rd at Gwendale Cres; Balaclava Rd opp Bligh St; Balaclava Rd at Corunna Rd; Balaclava Rd opp Irene Park; Balaclava Rd before Lincoln St; Balaclava Rd at Raymond St; Balaclava Rd after Abuklea Rd; Balaclava Rd after Agincourt Rd; Daughters of Charity, Balaclava Rd; Balaclava Rd before Epping Rd; Balaclava Rd at Hadenfield Ave; Macquarie University Campus University Ave; Macquarie Centre; Macquarie University Station Waterloo Rd; Waterloo Rd opp Byfield St; Waterloo Rd at Khartoum Rd; Waterloo Rd opp Coolinga St; Macquarie Park Station Waterloo Rd; Macquarie Park Station Waterloo Rd",
               "serviceDirection" : "Macquarie Park",
               "realtimeVehicle" : {
                  "components" : [
                     [
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "wifi" : false,
                           "airConditioned" : true,
                           "wheelchairAccessible" : true,
                           "occupancyText" : "Space available",
                           "model" : "Volvo~B8RLE~Volgren~OPTIMUS",
                           "wheelchairSeats" : 1
                        }
                     ]
                  ],
                  "id" : "969130",
                  "wifi" : false,
                  "occupancy" : "MANY_SEATS_AVAILABLE",
                  "location" : {
                     "lng" : 151.04555,
                     "bearing" : 119,
                     "lat" : -33.795
                  },
                  "lastUpdate" : 1594003979,
                  "label" : ""
               },
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "realTimeStatus" : "IS_REAL_TIME",
               "alertHashCodes" : [
                  981825199
               ],
               "serviceName" : "PrePay-Only - Parramatta to Macquarie Park",
               "serviceColor" : {
                  "blue" : 239,
                  "green" : 181,
                  "red" : 0
               },
               "operator" : "State Transit Sydney",
               "startTime" : 1594004880,
               "mode" : "bus",
               "operatorID" : "2439"
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "alt" : "bus",
                  "identifier" : "pt_pub_bus",
                  "localIcon" : "bus"
               },
               "serviceTripID" : "969131",
               "serviceNumber" : "545",
               "realtimeVehicle" : {
                  "lastUpdate" : 1594003993,
                  "label" : "",
                  "occupancy" : "MANY_SEATS_AVAILABLE",
                  "location" : {
                     "bearing" : 358,
                     "lat" : -33.81582,
                     "lng" : 151.00591
                  },
                  "components" : [
                     [
                        {
                           "wheelchairSeats" : 1,
                           "occupancyText" : "Space available",
                           "model" : "Volvo~B12BLEA~Custom Coaches~CB60EVO2",
                           "wheelchairAccessible" : true,
                           "airConditioned" : true,
                           "wifi" : false,
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        }
                     ]
                  ],
                  "id" : "969131",
                  "wifi" : false
               },
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "searchString" : "Eastwood Station West Pde; Eastwood Station Railway Pde; May St before Blaxland Rd; Moore Park, Balaclava Rd; Balaclava Rd opp Gordon St; Balaclava Rd at Gwendale Cres; Balaclava Rd opp Bligh St; Balaclava Rd at Corunna Rd; Balaclava Rd opp Irene Park; Balaclava Rd before Lincoln St; Balaclava Rd at Raymond St; Balaclava Rd after Abuklea Rd; Balaclava Rd after Agincourt Rd; Daughters of Charity, Balaclava Rd; Balaclava Rd before Epping Rd; Balaclava Rd at Hadenfield Ave; Macquarie University Campus University Ave; Macquarie Centre; Macquarie University Station Waterloo Rd; Waterloo Rd opp Byfield St; Waterloo Rd at Khartoum Rd; Waterloo Rd opp Coolinga St; Macquarie Park Station Waterloo Rd; Macquarie Park Station Waterloo Rd",
               "serviceDirection" : "Macquarie Park",
               "alertHashCodes" : [
                  981825199
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "operatorID" : "2439",
               "mode" : "bus",
               "startTime" : 1594005780,
               "operator" : "State Transit Sydney",
               "serviceColor" : {
                  "red" : 0,
                  "blue" : 239,
                  "green" : 181
               },
               "serviceName" : "PrePay-Only - Parramatta to Macquarie Park"
            },
            {
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "searchString" : "Eastwood Station West Pde; Eastwood Station Railway Pde; May St before Blaxland Rd; Moore Park, Balaclava Rd; Balaclava Rd opp Gordon St; Balaclava Rd at Gwendale Cres; Balaclava Rd opp Bligh St; Balaclava Rd at Corunna Rd; Balaclava Rd opp Irene Park; Balaclava Rd before Lincoln St; Balaclava Rd at Raymond St; Balaclava Rd after Abuklea Rd; Balaclava Rd after Agincourt Rd; Daughters of Charity, Balaclava Rd; Balaclava Rd before Epping Rd; Balaclava Rd at Hadenfield Ave; Macquarie University Campus University Ave; Macquarie Centre; Macquarie University Station Waterloo Rd; Waterloo Rd opp Byfield St; Waterloo Rd at Khartoum Rd; Waterloo Rd opp Coolinga St; Macquarie Park Station Waterloo Rd; Macquarie Park Station Waterloo Rd",
               "serviceDirection" : "Macquarie Park",
               "modeInfo" : {
                  "localIcon" : "bus",
                  "alt" : "bus",
                  "identifier" : "pt_pub_bus"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "969132",
               "serviceNumber" : "545",
               "mode" : "bus",
               "operatorID" : "2439",
               "serviceName" : "PrePay-Only - Parramatta to Macquarie Park",
               "serviceColor" : {
                  "green" : 181,
                  "blue" : 239,
                  "red" : 0
               },
               "operator" : "State Transit Sydney",
               "startTime" : 1594006680,
               "alertHashCodes" : [
                  981825199
               ],
               "realTimeStatus" : "IS_REAL_TIME"
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_bus",
                  "alt" : "bus",
                  "localIcon" : "bus"
               },
               "serviceNumber" : "545",
               "serviceTripID" : "969433",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "lastUpdate" : 1594003861,
                  "label" : "",
                  "location" : {
                     "lat" : -33.81697,
                     "bearing" : 282,
                     "lng" : 151.00301
                  },
                  "occupancy" : "MANY_SEATS_AVAILABLE",
                  "id" : "969433",
                  "wifi" : false,
                  "components" : [
                     [
                        {
                           "model" : "Volvo~B8RLE~Volgren~OPTIMUS",
                           "occupancyText" : "Space available",
                           "wheelchairSeats" : 1,
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "wheelchairAccessible" : true,
                           "airConditioned" : true,
                           "wifi" : false
                        }
                     ]
                  ]
               },
               "serviceDirection" : "Macquarie Park",
               "searchString" : "Eastwood Station West Pde; Eastwood Station Railway Pde; May St before Blaxland Rd; Moore Park, Balaclava Rd; Balaclava Rd opp Gordon St; Balaclava Rd at Gwendale Cres; Balaclava Rd opp Bligh St; Balaclava Rd at Corunna Rd; Balaclava Rd opp Irene Park; Balaclava Rd before Lincoln St; Balaclava Rd at Raymond St; Balaclava Rd after Abuklea Rd; Balaclava Rd after Agincourt Rd; Daughters of Charity, Balaclava Rd; Balaclava Rd before Epping Rd; Balaclava Rd at Hadenfield Ave; Macquarie University Campus University Ave; Macquarie Centre; Macquarie University Station Waterloo Rd; Waterloo Rd opp Byfield St; Waterloo Rd at Khartoum Rd; Waterloo Rd opp Coolinga St; Macquarie Park Station Waterloo Rd; Macquarie Park Station Waterloo Rd",
               "alertHashCodes" : [
                  981825199
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "operatorID" : "2439",
               "mode" : "bus",
               "startTime" : 1594007580,
               "operator" : "State Transit Sydney",
               "serviceColor" : {
                  "red" : 0,
                  "blue" : 239,
                  "green" : 181
               },
               "serviceName" : "PrePay-Only - Parramatta to Macquarie Park"
            },
            {
               "serviceColor" : {
                  "green" : 181,
                  "blue" : 239,
                  "red" : 0
               },
               "serviceName" : "PrePay-Only - Parramatta to Macquarie Park",
               "startTime" : 1594008480,
               "operator" : "State Transit Sydney",
               "mode" : "bus",
               "operatorID" : "2439",
               "alertHashCodes" : [
                  981825199
               ],
               "serviceDirection" : "Macquarie Park",
               "searchString" : "Eastwood Station West Pde; Eastwood Station Railway Pde; May St before Blaxland Rd; Moore Park, Balaclava Rd; Balaclava Rd opp Gordon St; Balaclava Rd at Gwendale Cres; Balaclava Rd opp Bligh St; Balaclava Rd at Corunna Rd; Balaclava Rd opp Irene Park; Balaclava Rd before Lincoln St; Balaclava Rd at Raymond St; Balaclava Rd after Abuklea Rd; Balaclava Rd after Agincourt Rd; Daughters of Charity, Balaclava Rd; Balaclava Rd before Epping Rd; Balaclava Rd at Hadenfield Ave; Macquarie University Campus University Ave; Macquarie Centre; Macquarie University Station Waterloo Rd; Waterloo Rd opp Byfield St; Waterloo Rd at Khartoum Rd; Waterloo Rd opp Coolinga St; Macquarie Park Station Waterloo Rd; Macquarie Park Station Waterloo Rd",
               "serviceTextColor" : {
                  "green" : 255,
                  "blue" : 255,
                  "red" : 255
               },
               "serviceNumber" : "545",
               "serviceTripID" : "969434",
               "modeInfo" : {
                  "identifier" : "pt_pub_bus",
                  "alt" : "bus",
                  "localIcon" : "bus"
               },
               "wheelchairAccessible" : true
            }
         ],
         "wheelchairAccessible" : true,
         "stopCode" : "2122122"
      },
      {
         "services" : [
            {
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     }
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "bearing" : 26,
                        "lat" : -33.75104,
                        "lng" : 150.61253
                     },
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lng" : 151.07608,
                        "lat" : -33.86466,
                        "bearing" : 283
                     },
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "lastUpdate" : 1594003217,
                     "label" : "",
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04"
                  },
                  {
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     },
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003822,
                     "label" : "",
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "lng" : 151.19768,
                        "lat" : -33.55813,
                        "bearing" : 236
                     }
                  }
               ],
               "operator" : "Sydney Trains",
               "startTime" : 1594004790,
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line",
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "realTimeDeparture" : 1594004790,
               "serviceTripID" : "138L.1382.133.4.A.8.62454659",
               "serviceNumber" : "T9",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "searchString" : "Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceDirection" : "Central, then Berowra",
               "realtimeVehicle" : {
                  "id" : "138L.1382",
                  "location" : {
                     "lat" : -33.73791,
                     "bearing" : 222,
                     "lng" : 151.0726
                  },
                  "components" : [
                     [
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        }
                     ]
                  ],
                  "label" : "12:46 Hornsby Station to Central Station ",
                  "lastUpdate" : 1594003926
               },
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               }
            },
            {
               "realtimeVehicleAlternatives" : [
                  {
                     "label" : "",
                     "lastUpdate" : 1594003919,
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "location" : {
                        "bearing" : 221,
                        "lat" : -34.10328,
                        "lng" : 150.99471
                     },
                     "id" : "NonTimetabled.1921",
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     },
                     "id" : "NonTimetabled.1877",
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     }
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "bearing" : 307,
                        "lat" : -33.86336,
                        "lng" : 151.05457
                     }
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04"
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     },
                     "id" : "NonTimetabled.T251"
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     }
                  },
                  {
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "startTime" : 1594005690,
               "operator" : "Sydney Trains",
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "T9",
               "serviceTripID" : "126J.1382.133.12.A.8.62450896",
               "realTimeDeparture" : 1594005690,
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "realtimeVehicle" : {
                  "location" : {
                     "lng" : 151.09827,
                     "lat" : -33.70317
                  },
                  "id" : "126J.1382",
                  "label" : "13:01 Hornsby Station to Central Station ",
                  "lastUpdate" : 1594003780
               },
               "serviceDirection" : "Central, then Hornsby",
               "searchString" : "Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station"
            },
            {
               "realTimeDeparture" : 1594006590,
               "serviceNumber" : "T9",
               "serviceTripID" : "144L.1382.133.4.A.8.62455317",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "serviceDirection" : "Central, then Berowra",
               "searchString" : "Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "label" : "",
                     "lastUpdate" : 1594003919,
                     "location" : {
                        "bearing" : 90,
                        "lat" : -33.747,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "bearing" : 221,
                        "lat" : -34.10328,
                        "lng" : 150.99471
                     }
                  },
                  {
                     "lastUpdate" : 1594003900,
                     "label" : "",
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     },
                     "id" : "NonTimetabled.1877"
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lng" : 151.07608,
                        "bearing" : 283,
                        "lat" : -33.86466
                     },
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     },
                     "id" : "NonTimetabled.2126",
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "lng" : 151.19768,
                        "lat" : -33.55813,
                        "bearing" : 236
                     },
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "startTime" : 1594006590,
               "operator" : "Sydney Trains",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line",
               "operatorID" : "SydneyTrains",
               "mode" : "train"
            },
            {
               "searchString" : "Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Croydon Station; Ashfield Station; Redfern Station; Central Station",
               "serviceDirection" : "Central, then Hornsby, Central, Blacktown",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "realTimeDeparture" : 1594007502,
               "serviceTripID" : "143L.1382.133.12.A.8.62452937",
               "serviceNumber" : "T9",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "startTime" : 1594007502,
               "operator" : "Sydney Trains",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line",
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "bearing" : 90,
                        "lat" : -33.747,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "lastUpdate" : 1594003908,
                     "label" : "",
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     }
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     }
                  },
                  {
                     "location" : {
                        "lng" : 151.0753,
                        "lat" : -33.86473,
                        "bearing" : 95
                     },
                     "id" : "NonTimetabled.CA04",
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "location" : {
                        "lat" : -33.88278,
                        "bearing" : 278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251"
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "lastUpdate" : 1594003822,
                     "label" : "",
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "id" : "NonTimetabled.4190"
                  }
               ]
            },
            {
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "serviceDirection" : "Central, then Berowra",
               "searchString" : "Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Croydon Station; Ashfield Station; Redfern Station; Central Station",
               "modeInfo" : {
                  "localIcon" : "train",
                  "identifier" : "pt_pub_train",
                  "alt" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "T9",
               "serviceTripID" : "115N.1382.133.12.T.8.62455866",
               "realTimeDeparture" : 1594008396,
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line",
               "startTime" : 1594008396,
               "operator" : "Sydney Trains",
               "realtimeVehicleAlternatives" : [
                  {
                     "label" : "",
                     "lastUpdate" : 1594003919,
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     },
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lng" : 151.07608,
                        "lat" : -33.86466,
                        "bearing" : 283
                     },
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lng" : 151.05457,
                        "bearing" : 307,
                        "lat" : -33.86336
                     },
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04"
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     },
                     "id" : "NonTimetabled.T251"
                  },
                  {
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     },
                     "id" : "NonTimetabled.CA07",
                     "label" : "",
                     "lastUpdate" : 1594003603
                  },
                  {
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME"
            }
         ],
         "stopCode" : "2122232",
         "wheelchairAccessible" : true
      },
      {
         "wheelchairAccessible" : true,
         "stopCode" : "2121221",
         "services" : [
            {
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "T9",
               "serviceTripID" : "138L.1382.133.4.A.8.62454659",
               "realTimeDeparture" : 1594004520,
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "label" : "12:46 Hornsby Station to Central Station ",
                  "lastUpdate" : 1594003926,
                  "location" : {
                     "lng" : 151.0726,
                     "bearing" : 222,
                     "lat" : -33.73791
                  },
                  "id" : "138L.1382",
                  "components" : [
                     [
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancyText" : "Space available",
                           "occupancy" : "MANY_SEATS_AVAILABLE"
                        },
                        {
                           "occupancy" : "MANY_SEATS_AVAILABLE",
                           "occupancyText" : "Space available"
                        }
                     ]
                  ]
               },
               "serviceDirection" : "Central, then Berowra",
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     }
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "location" : {
                        "lng" : 151.05457,
                        "bearing" : 307,
                        "lat" : -33.86336
                     },
                     "id" : "NonTimetabled.2126",
                     "lastUpdate" : 1594003399,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lng" : 151.0753,
                        "lat" : -33.86473,
                        "bearing" : 95
                     },
                     "id" : "NonTimetabled.CA04",
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "bearing" : 278,
                        "lat" : -33.88278
                     }
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "lastUpdate" : 1594003822,
                     "label" : "",
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     }
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line",
               "startTime" : 1594004520,
               "operator" : "Sydney Trains"
            },
            {
               "startTime" : 1594004700,
               "operator" : "NSW Trains",
               "serviceColor" : {
                  "blue" : 47,
                  "green" : 31,
                  "red" : 209
               },
               "serviceName" : "Central Coast & Newcastle Line",
               "operatorID" : "NSWTrains",
               "mode" : "train",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     },
                     "label" : "",
                     "lastUpdate" : 1594003919
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003900,
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     }
                  },
                  {
                     "location" : {
                        "lng" : 151.07608,
                        "bearing" : 283,
                        "lat" : -33.86466
                     },
                     "id" : "NonTimetabled.1BM9",
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "id" : "NonTimetabled.2126",
                     "label" : "",
                     "lastUpdate" : 1594003399
                  },
                  {
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04",
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003964,
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     }
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "location" : {
                        "lat" : -33.80713,
                        "bearing" : 203,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003822,
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     },
                     "id" : "NonTimetabled.4190"
                  }
               ],
               "serviceDirection" : "Central",
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "realtimeVehicle" : {
                  "id" : "242D.1382",
                  "location" : {
                     "bearing" : 202,
                     "lat" : -33.61368,
                     "lng" : 151.16008
                  },
                  "lastUpdate" : 1594003931,
                  "label" : "10:35 Newcastle Interchange Station to Central Station "
               },
               "serviceNumber" : "CCN",
               "serviceTripID" : "242D.1382.133.12.H.4.62450803",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               }
            },
            {
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "label" : "",
                     "lastUpdate" : 1594003919,
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.99471,
                        "bearing" : 221,
                        "lat" : -34.10328
                     },
                     "id" : "NonTimetabled.1921",
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "label" : "",
                     "lastUpdate" : 1594003900
                  },
                  {
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "lng" : 151.07608,
                        "lat" : -33.86466,
                        "bearing" : 283
                     },
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "location" : {
                        "bearing" : 307,
                        "lat" : -33.86336,
                        "lng" : 151.05457
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04",
                     "lastUpdate" : 1594003217,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "lastUpdate" : 1594003964,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lng" : 151.09024,
                        "bearing" : 203,
                        "lat" : -33.80713
                     },
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003822,
                     "label" : "",
                     "location" : {
                        "lng" : 151.19768,
                        "lat" : -33.55813,
                        "bearing" : 236
                     },
                     "id" : "NonTimetabled.4190"
                  }
               ],
               "operator" : "Sydney Trains",
               "startTime" : 1594005420,
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "realTimeDeparture" : 1594005420,
               "serviceNumber" : "T9",
               "serviceTripID" : "126J.1382.133.12.A.8.62450896",
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "serviceDirection" : "Central, then Hornsby",
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceTextColor" : {
                  "red" : 255,
                  "green" : 255,
                  "blue" : 255
               },
               "realtimeVehicle" : {
                  "location" : {
                     "lng" : 151.09827,
                     "lat" : -33.70317
                  },
                  "id" : "126J.1382",
                  "lastUpdate" : 1594003780,
                  "label" : "13:01 Hornsby Station to Central Station "
               }
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "realTimeDeparture" : 1594006320,
               "serviceTripID" : "144L.1382.133.4.A.8.62455317",
               "serviceNumber" : "T9",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceDirection" : "Central, then Berowra",
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "lat" : -33.747,
                        "bearing" : 90,
                        "lng" : 150.66479
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "lastUpdate" : 1594003610,
                     "label" : "",
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     }
                  },
                  {
                     "lastUpdate" : 1594003900,
                     "label" : "",
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     }
                  },
                  {
                     "location" : {
                        "lng" : 151.07608,
                        "lat" : -33.86466,
                        "bearing" : 283
                     },
                     "id" : "NonTimetabled.1BM9",
                     "lastUpdate" : 1594003908,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003217,
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04"
                  },
                  {
                     "location" : {
                        "lat" : -33.88278,
                        "bearing" : 278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     }
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "operator" : "Sydney Trains",
               "startTime" : 1594006320,
               "serviceName" : "T9 Northern Line",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               }
            },
            {
               "serviceName" : "Central Coast & Newcastle Line",
               "serviceColor" : {
                  "blue" : 47,
                  "green" : 31,
                  "red" : 209
               },
               "startTime" : 1594006500,
               "operator" : "NSW Trains",
               "mode" : "train",
               "operatorID" : "NSWTrains",
               "bicycleAccessible" : true,
               "realtimeVehicleAlternatives" : [
                  {
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "id" : "NonTimetabled.CA96",
                     "label" : "",
                     "lastUpdate" : 1594003919
                  },
                  {
                     "location" : {
                        "bearing" : 221,
                        "lat" : -34.10328,
                        "lng" : 150.99471
                     },
                     "id" : "NonTimetabled.1921",
                     "lastUpdate" : 1594003610,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "bearing" : 26,
                        "lat" : -33.75104,
                        "lng" : 150.61253
                     },
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003908,
                     "label" : "",
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "id" : "NonTimetabled.1BM9"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003399,
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     },
                     "id" : "NonTimetabled.2126"
                  },
                  {
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     },
                     "id" : "NonTimetabled.CA04",
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003964,
                     "id" : "NonTimetabled.T251",
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     }
                  },
                  {
                     "lastUpdate" : 1594003603,
                     "label" : "",
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lng" : 151.09024,
                        "lat" : -33.80713,
                        "bearing" : 203
                     }
                  },
                  {
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "serviceDirection" : "Central",
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "realtimeVehicle" : {
                  "lastUpdate" : 1594003818,
                  "label" : "11:24 Newcastle Interchange Station to Central Station ",
                  "id" : "N146.1382",
                  "location" : {
                     "lng" : 151.25697,
                     "lat" : -33.49211,
                     "bearing" : 225
                  }
               },
               "serviceNumber" : "CCN",
               "serviceTripID" : "N146.1382.133.4.V.8.62451642",
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "wheelchairAccessible" : true
            },
            {
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "serviceDirection" : "Central, then Hornsby, Central, Blacktown",
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Croydon Station; Ashfield Station; Redfern Station; Central Station",
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "wheelchairAccessible" : true,
               "serviceNumber" : "T9",
               "serviceTripID" : "143L.1382.133.12.A.8.62452937",
               "realTimeDeparture" : 1594007226,
               "mode" : "train",
               "operatorID" : "SydneyTrains",
               "serviceColor" : {
                  "red" : 209,
                  "blue" : 47,
                  "green" : 31
               },
               "serviceName" : "T9 Northern Line",
               "startTime" : 1594007226,
               "operator" : "Sydney Trains",
               "realtimeVehicleAlternatives" : [
                  {
                     "lastUpdate" : 1594003919,
                     "label" : "",
                     "location" : {
                        "lng" : 150.66479,
                        "lat" : -33.747,
                        "bearing" : 90
                     },
                     "id" : "NonTimetabled.CA96"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "location" : {
                        "lng" : 150.99471,
                        "lat" : -34.10328,
                        "bearing" : 221
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "lastUpdate" : 1594003900,
                     "label" : "",
                     "id" : "NonTimetabled.1877",
                     "location" : {
                        "lng" : 150.61253,
                        "lat" : -33.75104,
                        "bearing" : 26
                     }
                  },
                  {
                     "location" : {
                        "lat" : -33.86466,
                        "bearing" : 283,
                        "lng" : 151.07608
                     },
                     "id" : "NonTimetabled.1BM9",
                     "label" : "",
                     "lastUpdate" : 1594003908
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lng" : 151.05457,
                        "bearing" : 307,
                        "lat" : -33.86336
                     }
                  },
                  {
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04",
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251",
                     "label" : "",
                     "lastUpdate" : 1594003964
                  },
                  {
                     "location" : {
                        "bearing" : 203,
                        "lat" : -33.80713,
                        "lng" : 151.09024
                     },
                     "id" : "NonTimetabled.CA07",
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "lat" : -33.55813,
                        "bearing" : 236,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "label" : "",
                     "lastUpdate" : 1594003822
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME"
            },
            {
               "wheelchairAccessible" : true,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "realTimeDeparture" : 1594008120,
               "serviceTripID" : "115N.1382.133.12.T.8.62455866",
               "serviceNumber" : "T9",
               "serviceTextColor" : {
                  "red" : 255,
                  "blue" : 255,
                  "green" : 255
               },
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Croydon Station; Ashfield Station; Redfern Station; Central Station",
               "serviceDirection" : "Central, then Berowra",
               "realtimeVehicleAlternatives" : [
                  {
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     },
                     "lastUpdate" : 1594003919,
                     "label" : ""
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "id" : "NonTimetabled.1921",
                     "location" : {
                        "bearing" : 221,
                        "lat" : -34.10328,
                        "lng" : 150.99471
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.61253,
                        "bearing" : 26,
                        "lat" : -33.75104
                     },
                     "id" : "NonTimetabled.1877",
                     "lastUpdate" : 1594003900,
                     "label" : ""
                  },
                  {
                     "lastUpdate" : 1594003908,
                     "label" : "",
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "bearing" : 283,
                        "lat" : -33.86466,
                        "lng" : 151.07608
                     }
                  },
                  {
                     "lastUpdate" : 1594003399,
                     "label" : "",
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lat" : -33.86336,
                        "bearing" : 307,
                        "lng" : 151.05457
                     }
                  },
                  {
                     "lastUpdate" : 1594003217,
                     "label" : "",
                     "id" : "NonTimetabled.CA04",
                     "location" : {
                        "lng" : 151.0753,
                        "bearing" : 95,
                        "lat" : -33.86473
                     }
                  },
                  {
                     "location" : {
                        "lng" : 150.9939,
                        "lat" : -33.88278,
                        "bearing" : 278
                     },
                     "id" : "NonTimetabled.T251",
                     "lastUpdate" : 1594003964,
                     "label" : ""
                  },
                  {
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lng" : 151.09024,
                        "lat" : -33.80713,
                        "bearing" : 203
                     },
                     "lastUpdate" : 1594003603,
                     "label" : ""
                  },
                  {
                     "location" : {
                        "bearing" : 236,
                        "lat" : -33.55813,
                        "lng" : 151.19768
                     },
                     "id" : "NonTimetabled.4190",
                     "lastUpdate" : 1594003822,
                     "label" : ""
                  }
               ],
               "realTimeStatus" : "IS_REAL_TIME",
               "bicycleAccessible" : true,
               "operatorID" : "SydneyTrains",
               "mode" : "train",
               "startTime" : 1594008120,
               "operator" : "Sydney Trains",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "serviceName" : "T9 Northern Line"
            },
            {
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "wheelchairAccessible" : true,
               "serviceTripID" : "283E.1382.133.4.H.8.62455772",
               "serviceNumber" : "CCN",
               "realTimeDeparture" : 1594008300,
               "serviceTextColor" : {
                  "blue" : 255,
                  "green" : 255,
                  "red" : 255
               },
               "searchString" : "Eastwood Station; Denistone Station; West Ryde Station; Meadowbank Station; Rhodes Station; Concord West Station; North Strathfield Station; Strathfield Station; Burwood Station; Redfern Station; Central Station",
               "serviceDirection" : "Central",
               "realtimeVehicleAlternatives" : [
                  {
                     "id" : "NonTimetabled.CA96",
                     "location" : {
                        "lng" : 150.66479,
                        "bearing" : 90,
                        "lat" : -33.747
                     },
                     "label" : "",
                     "lastUpdate" : 1594003919
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003610,
                     "location" : {
                        "bearing" : 221,
                        "lat" : -34.10328,
                        "lng" : 150.99471
                     },
                     "id" : "NonTimetabled.1921"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003900,
                     "location" : {
                        "lat" : -33.75104,
                        "bearing" : 26,
                        "lng" : 150.61253
                     },
                     "id" : "NonTimetabled.1877"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003908,
                     "id" : "NonTimetabled.1BM9",
                     "location" : {
                        "bearing" : 283,
                        "lat" : -33.86466,
                        "lng" : 151.07608
                     }
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003399,
                     "id" : "NonTimetabled.2126",
                     "location" : {
                        "lng" : 151.05457,
                        "lat" : -33.86336,
                        "bearing" : 307
                     }
                  },
                  {
                     "location" : {
                        "bearing" : 95,
                        "lat" : -33.86473,
                        "lng" : 151.0753
                     },
                     "id" : "NonTimetabled.CA04",
                     "label" : "",
                     "lastUpdate" : 1594003217
                  },
                  {
                     "lastUpdate" : 1594003964,
                     "label" : "",
                     "location" : {
                        "bearing" : 278,
                        "lat" : -33.88278,
                        "lng" : 150.9939
                     },
                     "id" : "NonTimetabled.T251"
                  },
                  {
                     "label" : "",
                     "lastUpdate" : 1594003603,
                     "id" : "NonTimetabled.CA07",
                     "location" : {
                        "lat" : -33.80713,
                        "bearing" : 203,
                        "lng" : 151.09024
                     }
                  },
                  {
                     "lastUpdate" : 1594003822,
                     "label" : "",
                     "id" : "NonTimetabled.4190",
                     "location" : {
                        "lng" : 151.19768,
                        "bearing" : 236,
                        "lat" : -33.55813
                     }
                  }
               ],
               "bicycleAccessible" : true,
               "realTimeStatus" : "IS_REAL_TIME",
               "mode" : "train",
               "operatorID" : "NSWTrains",
               "serviceName" : "Central Coast & Newcastle Line",
               "serviceColor" : {
                  "red" : 209,
                  "green" : 31,
                  "blue" : 47
               },
               "startTime" : 1594008300,
               "operator" : "NSW Trains"
            }
         ]
      }
   ],
   "stops" : [
      {
         "children" : [
            {
               "timezone" : "Australia/Sydney",
               "lng" : 151.08209,
               "stopType" : "train",
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "region" : "AU_NSW_Sydney",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2122234",
               "address" : "Eastwood Station Platform 4",
               "lat" : -33.79014,
               "name" : "Eastwood Station Platform 4",
               "code" : "2122234",
               "shortName" : "Platform 4",
               "services" : "CCN, NRC, NRW, T9",
               "class" : "StopLocation",
               "stopCode" : "2122234",
               "popularity" : 2650
            },
            {
               "code" : "2122231",
               "name" : "Eastwood Station Platform 1",
               "lat" : -33.79015,
               "popularity" : 5550,
               "stopCode" : "2122231",
               "services" : "T1, T9, CCN, NRC, NRW",
               "class" : "StopLocation",
               "shortName" : "Platform 1",
               "region" : "AU_NSW_Sydney",
               "id" : "pt_pub|AU_NSW_Sydney|2122231",
               "wheelchairAccessible" : true,
               "address" : "Eastwood Station Platform 1",
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "lng" : 151.0824,
               "timezone" : "Australia/Sydney"
            },
            {
               "id" : "pt_pub|AU_NSW_Sydney|2122233",
               "wheelchairAccessible" : true,
               "address" : "Eastwood Station Platform 3",
               "region" : "AU_NSW_Sydney",
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08219,
               "code" : "2122233",
               "name" : "Eastwood Station Platform 3",
               "lat" : -33.79014,
               "popularity" : 6855,
               "stopCode" : "2122233",
               "class" : "StopLocation",
               "services" : "T9, CCN, NRW",
               "shortName" : "Platform 3"
            },
            {
               "stopCode" : "2122232",
               "popularity" : 3945,
               "shortName" : "Platform 2",
               "class" : "StopLocation",
               "services" : "T9, CCN, NRW",
               "name" : "Eastwood Station Platform 2",
               "code" : "2122232",
               "lat" : -33.79015,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "id" : "pt_pub|AU_NSW_Sydney|2122232",
               "wheelchairAccessible" : true,
               "address" : "Eastwood Station Platform 2",
               "region" : "AU_NSW_Sydney",
               "lng" : 151.08232,
               "timezone" : "Australia/Sydney"
            }
         ],
         "stopType" : "train",
         "modeInfo" : {
            "identifier" : "pt_pub_train",
            "alt" : "train",
            "localIcon" : "train"
         },
         "id" : "pt_pub|AU_NSW_Sydney|212210",
         "address" : "Eastwood Station",
         "region" : "AU_NSW_Sydney",
         "timezone" : "Australia/Sydney",
         "lng" : 151.08224,
         "name" : "Eastwood Station",
         "code" : "212210",
         "lat" : -33.79015,
         "stopCode" : "212210",
         "popularity" : 19000,
         "class" : "StopLocation",
         "services" : ""
      },
      {
         "code" : "2122122",
         "name" : "Hillview Rd opp Marist College",
         "lat" : -33.78885,
         "popularity" : 155,
         "stopCode" : "2122122",
         "class" : "StopLocation",
         "services" : "545",
         "wheelchairAccessible" : true,
         "address" : "Hillview Rd opp Marist College",
         "id" : "pt_pub|AU_NSW_Sydney|2122122",
         "region" : "AU_NSW_Sydney",
         "stopType" : "bus",
         "modeInfo" : {
            "localIcon" : "bus",
            "alt" : "bus",
            "identifier" : "pt_pub_bus"
         },
         "timezone" : "Australia/Sydney",
         "lng" : 151.07752
      },
      {
         "region" : "AU_NSW_Sydney",
         "address" : "Epping Station",
         "id" : "pt_pub|AU_NSW_Sydney|212110",
         "stopType" : "train",
         "modeInfo" : {
            "localIcon" : "train",
            "identifier" : "pt_pub_train",
            "alt" : "train"
         },
         "children" : [
            {
               "lng" : 151.08229,
               "timezone" : "Australia/Sydney",
               "region" : "AU_NSW_Sydney",
               "id" : "pt_pub|AU_NSW_Sydney|2121225",
               "wheelchairAccessible" : true,
               "address" : "Epping Station Platform 5",
               "stopType" : "subway",
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "services" : "M",
               "class" : "StopLocation",
               "shortName" : "5",
               "popularity" : 7597,
               "stopCode" : "2121225",
               "lat" : -33.77291,
               "code" : "2121225",
               "name" : "Epping Station Platform 5"
            },
            {
               "lat" : -33.77298,
               "code" : "2121226",
               "name" : "Epping Station Platform 6",
               "class" : "StopLocation",
               "services" : "M",
               "shortName" : "6",
               "popularity" : 7610,
               "stopCode" : "2121226",
               "lng" : 151.08206,
               "timezone" : "Australia/Sydney",
               "address" : "Epping Station Platform 6",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2121226",
               "region" : "AU_NSW_Sydney",
               "modeInfo" : {
                  "alt" : "metro",
                  "identifier" : "pt_pub_metro",
                  "localIcon" : "subway"
               },
               "stopType" : "subway"
            },
            {
               "wheelchairAccessible" : true,
               "address" : "Epping Station Platform 1",
               "id" : "pt_pub|AU_NSW_Sydney|2121221",
               "region" : "AU_NSW_Sydney",
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08232,
               "popularity" : 7775,
               "stopCode" : "2121221",
               "class" : "StopLocation",
               "services" : "T1, T9, CCN, NRC, NRW",
               "shortName" : "Platform 1",
               "code" : "2121221",
               "name" : "Epping Station Platform 1",
               "lat" : -33.77295
            },
            {
               "timezone" : "Australia/Sydney",
               "lng" : 151.08218,
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "region" : "AU_NSW_Sydney",
               "address" : "Epping Station Platform 2",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2121222",
               "shortName" : "Platform 2",
               "services" : "T9, CCN",
               "class" : "StopLocation",
               "stopCode" : "2121222",
               "popularity" : 6345,
               "lat" : -33.77299,
               "name" : "Epping Station Platform 2",
               "code" : "2121222"
            },
            {
               "region" : "AU_NSW_Sydney",
               "id" : "pt_pub|AU_NSW_Sydney|2121223",
               "wheelchairAccessible" : true,
               "address" : "Epping Station Platform 3",
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "timezone" : "Australia/Sydney",
               "lng" : 151.0819,
               "code" : "2121223",
               "name" : "Epping Station Platform 3",
               "lat" : -33.77265,
               "popularity" : 4300,
               "stopCode" : "2121223",
               "services" : "CCN, T9, NRC, NRW",
               "class" : "StopLocation",
               "shortName" : "Platform 3"
            }
         ],
         "lng" : 151.08211,
         "timezone" : "Australia/Sydney",
         "popularity" : 33627,
         "stopCode" : "212110",
         "services" : "",
         "class" : "StopLocation",
         "code" : "212110",
         "name" : "Epping Station",
         "lat" : -33.77282
      }
   ],
   "parentStops" : [
      {
         "lng" : 151.08224,
         "timezone" : "Australia/Sydney",
         "children" : [
            {
               "region" : "AU_NSW_Sydney",
               "id" : "pt_pub|AU_NSW_Sydney|2122234",
               "wheelchairAccessible" : true,
               "address" : "Eastwood Station Platform 4",
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08209,
               "popularity" : 2650,
               "stopCode" : "2122234",
               "services" : "CCN, NRC, NRW, T9",
               "class" : "StopLocation",
               "shortName" : "Platform 4",
               "code" : "2122234",
               "name" : "Eastwood Station Platform 4",
               "lat" : -33.79014
            },
            {
               "popularity" : 5550,
               "stopCode" : "2122231",
               "services" : "T1, T9, CCN, NRC, NRW",
               "class" : "StopLocation",
               "shortName" : "Platform 1",
               "code" : "2122231",
               "name" : "Eastwood Station Platform 1",
               "lat" : -33.79015,
               "region" : "AU_NSW_Sydney",
               "id" : "pt_pub|AU_NSW_Sydney|2122231",
               "wheelchairAccessible" : true,
               "address" : "Eastwood Station Platform 1",
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "lng" : 151.0824,
               "timezone" : "Australia/Sydney"
            },
            {
               "lng" : 151.08219,
               "timezone" : "Australia/Sydney",
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2122233",
               "address" : "Eastwood Station Platform 3",
               "region" : "AU_NSW_Sydney",
               "shortName" : "Platform 3",
               "class" : "StopLocation",
               "services" : "T9, CCN, NRW",
               "stopCode" : "2122233",
               "popularity" : 6855,
               "lat" : -33.79014,
               "name" : "Eastwood Station Platform 3",
               "code" : "2122233"
            },
            {
               "modeInfo" : {
                  "identifier" : "pt_pub_train",
                  "alt" : "train",
                  "localIcon" : "train"
               },
               "stopType" : "train",
               "address" : "Eastwood Station Platform 2",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2122232",
               "region" : "AU_NSW_Sydney",
               "lng" : 151.08232,
               "timezone" : "Australia/Sydney",
               "name" : "Eastwood Station Platform 2",
               "code" : "2122232",
               "lat" : -33.79015,
               "stopCode" : "2122232",
               "popularity" : 3945,
               "shortName" : "Platform 2",
               "class" : "StopLocation",
               "services" : "T9, CCN, NRW"
            }
         ],
         "stopType" : "train",
         "modeInfo" : {
            "localIcon" : "train",
            "alt" : "train",
            "identifier" : "pt_pub_train"
         },
         "region" : "AU_NSW_Sydney",
         "id" : "pt_pub|AU_NSW_Sydney|212210",
         "address" : "Eastwood Station",
         "services" : "",
         "class" : "StopLocation",
         "stopCode" : "212210",
         "popularity" : 19000,
         "lat" : -33.79015,
         "name" : "Eastwood Station",
         "code" : "212210"
      },
      {
         "lat" : -33.77282,
         "name" : "Epping Station",
         "code" : "212110",
         "services" : "",
         "class" : "StopLocation",
         "stopCode" : "212110",
         "popularity" : 33627,
         "timezone" : "Australia/Sydney",
         "lng" : 151.08211,
         "stopType" : "train",
         "children" : [
            {
               "stopCode" : "2121225",
               "popularity" : 7597,
               "shortName" : "5",
               "class" : "StopLocation",
               "services" : "M",
               "name" : "Epping Station Platform 5",
               "code" : "2121225",
               "lat" : -33.77291,
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "stopType" : "subway",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2121225",
               "address" : "Epping Station Platform 5",
               "region" : "AU_NSW_Sydney",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08229
            },
            {
               "code" : "2121226",
               "name" : "Epping Station Platform 6",
               "lat" : -33.77298,
               "popularity" : 7610,
               "stopCode" : "2121226",
               "class" : "StopLocation",
               "services" : "M",
               "shortName" : "6",
               "wheelchairAccessible" : true,
               "address" : "Epping Station Platform 6",
               "id" : "pt_pub|AU_NSW_Sydney|2121226",
               "region" : "AU_NSW_Sydney",
               "modeInfo" : {
                  "identifier" : "pt_pub_metro",
                  "alt" : "metro",
                  "localIcon" : "subway"
               },
               "stopType" : "subway",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08206
            },
            {
               "shortName" : "Platform 1",
               "services" : "T1, T9, CCN, NRC, NRW",
               "class" : "StopLocation",
               "stopCode" : "2121221",
               "popularity" : 7775,
               "lat" : -33.77295,
               "name" : "Epping Station Platform 1",
               "code" : "2121221",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08232,
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "stopType" : "train",
               "region" : "AU_NSW_Sydney",
               "address" : "Epping Station Platform 1",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2121221"
            },
            {
               "stopCode" : "2121222",
               "popularity" : 6345,
               "shortName" : "Platform 2",
               "services" : "T9, CCN",
               "class" : "StopLocation",
               "name" : "Epping Station Platform 2",
               "code" : "2121222",
               "lat" : -33.77299,
               "stopType" : "train",
               "modeInfo" : {
                  "alt" : "train",
                  "identifier" : "pt_pub_train",
                  "localIcon" : "train"
               },
               "region" : "AU_NSW_Sydney",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2121222",
               "address" : "Epping Station Platform 2",
               "timezone" : "Australia/Sydney",
               "lng" : 151.08218
            },
            {
               "name" : "Epping Station Platform 3",
               "code" : "2121223",
               "lat" : -33.77265,
               "stopCode" : "2121223",
               "popularity" : 4300,
               "shortName" : "Platform 3",
               "services" : "CCN, T9, NRC, NRW",
               "class" : "StopLocation",
               "modeInfo" : {
                  "localIcon" : "train",
                  "alt" : "train",
                  "identifier" : "pt_pub_train"
               },
               "stopType" : "train",
               "region" : "AU_NSW_Sydney",
               "wheelchairAccessible" : true,
               "id" : "pt_pub|AU_NSW_Sydney|2121223",
               "address" : "Epping Station Platform 3",
               "timezone" : "Australia/Sydney",
               "lng" : 151.0819
            }
         ],
         "modeInfo" : {
            "localIcon" : "train",
            "identifier" : "pt_pub_train",
            "alt" : "train"
         },
         "region" : "AU_NSW_Sydney",
         "id" : "pt_pub|AU_NSW_Sydney|212110",
         "address" : "Epping Station"
      }
   ],
   "alerts" : [
      {
         "url" : "https://www.transportnsw.info/alerts#/6048981",
         "type" : "SERVICE_INFORMATION",
         "severity" : "warning",
         "title" : "Kissing Point Rd, Dundas bus stop relocation",
         "serviceTripID" : "969130",
         "text" : "From 5am Tuesday 14 April to the end of August 2020, route 545 bus services will not run from Vineyard Creek Reserve, Kissing Point Rd (stop ID 211767) due to temporary closures for the Parramatta Light Rail works. Route 545 will run from a temporary alternative stop or from Fred Robertson Park, Kissing Point Rd (Stop ID 211722).",
         "hashCode" : 981825199
      }
   ]
};
