import {Plugins} from '@capacitor/core';

const {Storage} = Plugins;
const cache     = {};

// doesn't save default to storage, but puts it in the cache for sync get
export const init = (key, def) => {
  (async () => {
    const value = (await Storage.get({key})).value;
    if (value !== null) {
      cache[key] = JSON.parse(value);
    } else {
      cache[key] = def;
    }
  })();
};

// be sure to call init first
export const get = (key) => {
  console.assert(key in cache);
  return cache[key];
};

export const set = (key, value) => {
  cache[key] = value;
  // noinspection JSIgnoredPromiseFromCall
  Storage.set({key, value: JSON.stringify(value)});
  return value;
}
