import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
import * as debounce from 'lodash.debounce';

export default class Geocoder extends MapboxGeocoder {
  constructor(options) {
    super({bbox: [149.5, -35.2, 152.67, -32], ...options});

    this.on('result', () => this._inputEl.blur());
  }

  onAdd(...args) {
    const addEventListener                 = EventTarget.prototype.addEventListener;
    EventTarget.prototype.addEventListener = function (type, ...args) {
      if (type !== 'keydown') {
        // the debouncing causes the options to open when a result is tapped
        addEventListener.call(this, type, ...args);
      }
    };
    const el = super.onAdd(...args);
    EventTarget.prototype.addEventListener = addEventListener;

    this._inputEl.addEventListener('input', debounce(this._onKeyDown, 200));
    this._inputEl.addEventListener('focus', () => {
      // does funny things on my android phone
      // this._inputEl.select();

      if (this._typeahead.data.length) {
        // if there was a list before, just display it, so the user can quickly choose a different result
        this._typeahead.update(this._typeahead.data);
      } else {
        // otherwise, get and show results
        const localGeocoderOnly        = this.options.localGeocoderOnly;
        this.options.localGeocoderOnly = !this._inputEl.value || this._inputEl.value === YOUR_LOCATION;
        if (!this.options.localGeocoderOnly || this.options.localGeocoder)
          this._geocode(this._inputEl.value);
        this.options.localGeocoderOnly = localGeocoderOnly;
      }
    });

    return el;
  }
}
export const YOUR_LOCATION = 'Your location';
