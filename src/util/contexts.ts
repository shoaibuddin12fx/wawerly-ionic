import React, {Dispatch, SetStateAction} from "react";

export const SetAnimatedContext = React.createContext<Dispatch<SetStateAction<boolean>>>(() => {
});
export const CacheContext = React.createContext({});
