import {init, get, set} from './storage';

init('favourites', {});

// have to use stop.code not stop.stopCode to be consistent with what is given to departures.json

/** @return {boolean} */
export const add = code => {
  const favourites = get('favourites');
  favourites[code] = true;
  set('favourites', favourites);
  return true;
};

/** @return {boolean} */
export const remove = code => {
  const favourites = get('favourites');
  favourites[code] = false;
  set('favourites', favourites);
  return false;
};

/** @return {string[]} */
export const toList = () => {
  const favourites = get('favourites');
  return Object.keys(favourites).filter(code => favourites[code]);
};

/** @return {boolean} */
export const includes = code => {
  const favourites = get('favourites');
  return !!favourites[code];
};
