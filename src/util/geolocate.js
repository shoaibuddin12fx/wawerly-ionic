import { Plugins } from '@capacitor/core';
import { isPlatform } from '@ionic/react';

import {get, init, set } from './storage';

const { Geolocation } = Plugins;

init('geolocatePermission', false);

/**
 * @param {PositionError|Error} error
 */
export function onGeolocateError(error) {
    // ionic throws a Error
    // html5 includes an error code
    if (error.message === 'User denied location permission' ||
        error.code === window.GeolocationPositionError.PERMISSION_DENIED) {
        set('geolocatePermission', false);
    }
}

/**
 * @param {Position} data
 */
export function onGeolocateSuccess(data) {
    set('geolocatePermission', true);
}

export function hasGeolocatePermission() {
    return get('geolocatePermission');
}

/** @return {Promise<number[]>} */
export async function getGeolocation() {
    try {
        const position = await Geolocation.getCurrentPosition();
        onGeolocateSuccess(position);
        return [position.coords.longitude, position.coords.latitude];
    } catch (error) {
        onGeolocateError(error);
    }
}

// mapboxgl.GeolocateControl uses these, but they don't work on capacitor ios
/*
 * Removed the code hack and testing on ios platform
 * if test completes, this code block will be removed
 *
 * //
if (isPlatform('ios') && !isPlatform('mobileweb')) {
  window.navigator.geolocation.watchPosition = (successCallback, errorCallback, options) => {
    try {
      return Geolocation.watchPosition(options, successCallback);
    } catch (error) {
      errorCallback(error);
      return -1;
    }
  };

  window.navigator.geolocation.clearWatch = id => Geolocation.clearWatch({id});

  window.navigator.geolocation.getCurrentPosition = async (successCallback, errorCallback, options) => {
    try {
      const position = await Geolocation.getCurrentPosition(options);
      successCallback(position);
    } catch (error) {
      errorCallback(error);
    }
  }
}
*/