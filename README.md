# Development

1. [Install Node.js](https://nodejs.org/en/download/package-manager/) (v12 is known to work as at 2020-07-13)
2. Run `npm install` from the project root. (Some scripts have trouble with yarn)
3. Run `bash resources.sh` to setup the app icon and splash screen
4. Run `npx ionic build` as some steps require the `build/` deployment directory even though it may not be used (e.g., in development)
5. On macOS, run `xcode-select --install` or somehow install the Xcode command line tools
6. On macOS, run `sudo gem install cocoapods` because you need the pod command
7. Run `npx capacitor update` to create some necessary platform files not in git

## Android

The purpose of this script is that it does not require Android Studio, just the Android SDK.

`bash android-debug.sh`

## iOS

* `npx ionic capacitor copy ios` to copy built files into xcode project

YAY.. nearly there. Now setup the xcode project...

* `cd ios/App`
* `pod install` this should only be needed once. You may need to [install cocoapods](https://cocoapods.org/)... 
* `open App.xcworkspace` to lauch xcode

You may also need to set the development team... To do this in Xcode, navigate to `Project -> App target -> Signing & capabilities -> Team:`

# Release

## Android

The purpose of this script is that it does not require Android Studio, just the Android SDK.

`bash android-production.sh`

# Update Waverley layers

1. download shapefiles manually from https://opendata.transport.nsw.gov.au/dataset/waverley-council-data to resources/shapefiles/
2. ```shell script
   cd public/assets/data/shapefiles/
   echo \{ > ../zip2shp.json
   for zip in *.zip; do
     unzip -u "$zip"
     shp="$(unzip -l "$zip" | grep shp$ | sed 's/.* //')"
     echo "\"$zip\":\"$shp\"," >> ../zip2shp.json
   done
   truncate -s-2 ../zip2shp.json
   echo \} >> ../zip2shp.json
   cd -
   ```
3. ```shell script
   cd public/assets/data/
   for shp in shapefiles/*.shp; do
     geojson="${shp/shapefiles/geojson}"
     geojson="${geojson/.shp/.json}"
     yarn run mapshaper "$shp" -proj wgs84 -o format=geojson "$geojson"
   done
   cd -
   mv public/assets/data/shapefiles resources/
   ```   
4. `curl https://opendata.transport.nsw.gov.au/api/3/action/package_show?id=5a6d694f-fbbf-49d9-a147-b7d7b00c8013 | json_xs > package.json`
5. `node --harmony-optional-chaining geojson.js`

# Update charging layer

## OCM

1. `cd public/assets/data`
2. `curl --user-agent 'Waverley Transport' --header X-API-Key:b63a43e3-21dc-4ca8-8b48-8c0dd91f64be 'https://api.openchargemap.io/v3/poi/?output=json&maxresults=10&polygon=%60e%7CpEggoq%5B%3Ff%60%60AaerM%3F%3FmcjRnwyR%3F%3FdbiP' | tee ocm.json`
3. `cd -`
4. `node --harmony-optional-chaining geojson.js`

## OSM

1. `cd public/assets/data`
2. `curl 'http://overpass-api.de/api/interpreter?data=%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3B%28node%5B%22amenity%22%3D%22charging_station%22%5D%28-35.2%2C149.5%2C-32%2C152.67%29%3Bway%5B%22amenity%22%3D%22charging_station%22%5D%28-34.7009774147201%2C149.99359130859372%2C-33.06852769197118%2C152.4078369140625%29%3Brelation%5B%22amenity%22%3D%22charging_station%22%5D%28-34.7009774147201%2C149.99359130859372%2C-33.06852769197118%2C152.4078369140625%29%3B%29%3Bout%20body%3B%3E%3Bout%20skel%20qt%3B' | tee osm.json`
3. `cd -`
4. `node --harmony-optional-chaining geojson.js`

# Ionic React Conference Application


This application is purely a kitchen-sink demo of the Ionic Framework and React.

**There is not an actual Ionic Conference at this time.** This project is just to show off Ionic components in a real-world application.

## Table of Contents
- [Getting Started](#getting-started)
- [App Preview](#app-preview)

## Getting Started

* [Download the installer](https://nodejs.org/) for Node LTS.
* Install the ionic CLI globally: `npm install -g ionic`
* Clone this repository: `git clone https://github.com/ionic-team/ionic-react-conference-app.git`.
* Run `npm install` from the project root.
* Run `ionic serve` in a terminal from the project root.
* Profit. :tada:

## App Preview

### [Menu](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/menu/menu.html)

| Material Design  | iOS  |
| -----------------| -----|
| ![Android Menu](/resources/screenshots/android-menu.png) | ![iOS Menu](/resources/screenshots/ios-menu.png) |


### [Schedule Page](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/schedule/schedule.html)

| Material Design  | iOS  |
| -----------------| -----|
| ![Android Schedule](/resources/screenshots/android-schedule.png) | ![iOS Schedule](/resources/screenshots/ios-schedule.png) |

### [Speakers Page](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/speaker-list/speaker-list.html)

| Material Design  | iOS  |
| -----------------| -----|
| ![Android Speakers](/resources/screenshots/android-speakers.png) | ![iOS Speakers](/resources/screenshots/ios-speakers.png) |

### [Speaker Detail Page](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/speaker-detail/speaker-detail.html)

| Material Design  | iOS  |
| -----------------| -----|
| ![Android Speaker Detail](/resources/screenshots/android-speaker-detail.png) | ![iOS Speaker Detail](/resources/screenshots/ios-speaker-detail.png) |

### [About Page](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/about/about.html)

| Material Design  | iOS  |
| -----------------| -----|
| ![Android About](/resources/screenshots/android-about.png) | ![iOS About](/resources/screenshots/ios-about.png) |

