#!/bin/bash

PHONE_IP=10.27.164.118

set -e
set -u

SERVER_IP=$(ifdata -pa wlan0)
PATH="$ANDROID_SDK_ROOT/platform-tools:$PATH"

if ! curl $SERVER_IP:8100 --output /dev/null; then
  # https uses a self-signed certificate, which doesn't work with capacitor
  npx ionic capacitor run android --external --livereload --no-open --public-host=$SERVER_IP &
fi
while ! curl $SERVER_IP:8100 --output /dev/null; do
  sleep 1
done
cd android
[[ $(adb devices | wc -l) -gt 3 ]] && adb disconnect $PHONE_IP:5555
# in case USB is flaky, start a network server with `adb tcpip 5555`
adb devices | grep device$ || adb connect $PHONE_IP:5555
#adb uninstall waverley.transport.maps.transit.timetable.debug
./gradlew --no-daemon installDebug
cd -
adb shell am start -n waverley.transport.maps.transit.timetable.debug/waverley.transport.maps.transit.timetable.MainActivity -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
